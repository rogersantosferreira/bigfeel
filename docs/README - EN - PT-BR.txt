-PORTUG�S BRASIL-
C�digo-fonte do projeto de mestrado em Ci�ncia da Computa��o pela UFLA, de Roger Santos Ferreira, sob orienta��o do professor Denilson Alves Pereira. BigFeel, uma abordagem para a integra��o de ferramentas de an�lise de sentimentos em ambiente distribu�do.

-ENGLISH-
Source-code of master's degreee project in Computer Science by Roger Santos Ferreira (UFLA), under orientation of professor Denilson Alves Pereira. BigFeel, an approach to integrate tools for sentiment analysis in a distributed environment.