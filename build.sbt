name := "BigFeel"

version := "1.0"

scalaVersion := "2.11.8"

javaOptions += "-Xmx4096m"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.1" % "provided"
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.1.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.3.0" % "provided"

/*assemblyShadeRules in assembly := Seq(
      ShadeRule.rename("edu.stanford.nlp.**" -> "shadeStanford.@1").inAll
    )*/

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}