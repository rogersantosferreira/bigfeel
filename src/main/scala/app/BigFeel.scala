/* Version 1.0 - Roger Santos Ferreira - October, 2018
 * 
 * How to create/update BigFeel project dependencies + buildpath BigFeel on Eclipse Neon
 *  1) Uses sbt plugin
 *  2) Install the sbt eclipse plugin
 *  3) On the root of BigFeel project, use sbt clean, sbt reload, sbt eclipse
 *  4) Update the eclipse folder to verify the results
 *  5) Configure the buildpath and add (Add Jar) not externally, all the content of lib folder
 *  6) IMPORTANT: In configure BuildPath, select the jar of Ifeel and send him to the end of list (Order and Export)  
 *  7) IMPORTANT 2: select the jar of CLULab and put him on top of the list
 *  
 * How to make build with sbt:
 * 	1) Save project in eclipse (without .master([*]) and without a copy of resources for workers)
 * 	2) Execute cmd/console at root folder of project
 * 	3) Execute commands:
 * 		a) sbt clean
 * 	  b) sbt reload
 *    c) set JAVA_OPTS=-Xmx4G (Windows) or export SBT_OPTS="-Xmx4G" (Linux) to increase heap of JVM 
 * 		c) sbt assembly
 * 	4) Take the jar file generated in target folder as your final bigfeel jar file
 * 	5) To treat duplicated files in assembly: add instructions of merge in build.sbt https://stackoverflow.com/questions/25144484/sbt-assembly-deduplication-found-error
 * 
 * To run application:
 *  1) Execute script zeracluster to clean all the use history on the cluster, as well the data from HDFS (or resetcluster)
 *  2) Copy and paste de resources folder in HDFS: hadoop fs -put resources /
 *  3) Run the script of experimentation: 
 *     (ex.:) experimentos_spark_local, or 
 *     spark-submit --class app.BigFeel --driver-memory 4G --executor-memory 4G --master local[*] BigFeel-assembly-1.0.jar data/ifeel/amazon.txt Afinn
 *  
 */

package app

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.ml.feature.Tokenizer
//import adapters._
//import com.lg.sentiment.MethodCreator
//import org.apache.spark.sql.functions._
//import com.databricks.spark.corenlp.functions._

object BigFeel {
  
  //main method of application
  def main(args: Array[String]): Unit = {
    
    // set the level of logs only for erros
    Logger.getLogger("org").setLevel(Level.ERROR)
  
    //parameters validation
    if (args.length == 0) {
        println("Invalid parameters to run the application!")
        println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Afinn")
        System.exit(1)
    }
    
    //EXECUTION PARAMETERS ============================================================================================================    
    val input = args(0)                                          //input dataset definition
    val method_parameter = args(1)                               //method definition
    var param_methods = ""                                       //methods for composition
    var param_weights = ""                                       //weights for composition
    // ================================================================================================================================ 
    
    // starts de Spark Session
    val spark = SparkSession
        .builder()
        .appName("BigFeel" + "-" + input + "-" + method_parameter)
        //.master("local[*]")                                      //unrequired to cluster run - just for debug
        //.config()
        .getOrCreate()  
    
    //addition of jars to the classpath                          //necessary to cluster run
    val sc = spark.sparkContext
    val master = sc.master
    if (master != "local[*]") {                                  //if running in a cluster, needs the resources folder clonned into each slave nodes
      val url_master = master.split(":")(1)                      //get just the //10.120.1.50 from master, passing via command line
      sc.addFile("hdfs:"+ url_master +":9000/resources", true)   //copy and paste the entire folder of resources from HDFS recursivelly (true) for each node  
    }
    
    //imports in loco
    import spark.implicits._                                     //allows the use of 'sentence like dataframe.col("sentence"), bitwise implicity conversions from RDD to DF and the contrary
    
    //DATA INPUT ======================================================================================================================
    val data = spark.read.format("text").load(input).toDF("sentence")
    
    /*import adapters.AfinnTransformer
    val teste = new AfinnTransformer()
                .setInputCol("sentence")
                .setOutputCol("Afinn") 
                .transform(dados).show()
    return*/
    
    //TEXT PREPROCESSING ==============================================================================================================
    /*
    //UDF for untokenize string array os terms
    val untokenizer = spark.udf.register("untokenizer", (input: Seq[String]) => { input.mkString(" ") })
    val tokenizer = new Tokenizer()                             //separate tokens and normalize the terms
                    .setInputCol("sentenca")
                    .setOutputCol("words")
    val dados2 = tokenizer.transform(dados)
                    
    val remover = new StopWordsRemover()												//remove stopwords
                  .setInputCol("words")
                  .setOutputCol("filtered")
    val dados3 = remover.transform(dados2)
    val dados4 = dados3.withColumn("preprocessed_text", untokenizer(dados3.col("filtered")))
    dados4.show()
    return
    */
    
    var result: Option[DataFrame] = None                       //result variable definition
    
    // UDFs ===========================================================================================================================
    // String result conversions
    import org.apache.spark.sql.functions._
    val stringify = udf((vs: Seq[String]) => s"""[${vs.flatMap(Option[String]).mkString(",")}]""")    //https://stackoverflow.com/questions/40426106/spark-2-0-x-dump-a-csv-file-from-a-dataframe-containing-one-array-of-type-string
    val stringify2 = udf((ele: Any) => ele.toString())                        //generic - converts any object to string
    
    //METHODS OF BIGFEEL ==============================================================================================================
    method_parameter match {
      
      // IFEEL ========================================================================================================================
      case "Afinn"                => import adapters.Afinn
                                     val afinn = spark.udf.register("Method", (input: String) => { Afinn.as(input) })
                                     result = Some(data.withColumn("analysis", afinn(data.col("sentence"))))
      
      case "Emolex"               => import adapters.Emolex
                                     val emolex = spark.udf.register("Method", (input: String) => { Emolex.as(input) })
                                     result = Some(data.withColumn("analysis", emolex(data.col("sentence"))))
      
      case "Emoticons"            => import adapters.Emoticons
                                     val emoticons = spark.udf.register("Method", (input: String) => { Emoticons.as(input) })
                                     result = Some(data.withColumn("analysis", emoticons(data.col("sentence"))))
      
      case "EmoticonDS"           => import adapters.EmoticonDS
                                     val emoticonDS = spark.udf.register("Method", (input: String) => { EmoticonDS.as(input) })
                                     result = Some(data.withColumn("analysis", emoticonDS(data.col("sentence"))))
      
      case "HappinessIndex"       => import adapters.HappinessIndex
                                     val happiness = spark.udf.register("Method", (input: String) => { HappinessIndex.as(input) })
                                     result = Some(data.withColumn("analysis", happiness(data.col("sentence"))))

      case "MPQA"                 => import adapters.MPQA
                                     val mpqa = spark.udf.register("Method", (input: String) => { MPQA.as(input) })
                                     result = Some(data.withColumn("analysis", mpqa(data.col("sentence"))))

      case "NRCHashtag"           => import adapters.NRC
                                     val nrc = spark.udf.register("Method", (input: String) => { NRC.as(input) })
                                     result = Some(data.withColumn("analysis", nrc(data.col("sentence"))))
      
      case "OpinionLexicon"       => import adapters.Opinion
                                     val opinion = spark.udf.register("Method", (input: String) => { Opinion.as(input) })
                                     result = Some(data.withColumn("analysis", opinion(data.col("sentence"))))
      
      case "PanasT"               => import adapters.PanasT
                                     val panasT = spark.udf.register("Method", (input: String) => { PanasT.as(input) })
                                     result = Some(data.withColumn("analysis", panasT(data.col("sentence"))))

      case "Sann"                 => import adapters.Sann
                                     val sann = spark.udf.register("Method", (input: String) => { Sann.as(input) })
                                     result = Some(data.withColumn("analysis", sann(data.col("sentence"))))
      
      case "Sasa"                 => import adapters.Sasa
                                     val sasa = spark.udf.register("Method", (input: String) => { Sasa.as(input) })
                                     result = Some(data.withColumn("analysis", sasa(data.col("sentence"))))
      
      case "SenticNet"            => import adapters.SenticNet
                                     val senticNet = spark.udf.register("Method", (input: String) => { SenticNet.as(input) })
                                     result = Some(data.withColumn("analysis", senticNet(data.col("sentence"))))

      case "Sentiment140"         => import adapters.Sentiment140
                                     val sentiment140 = spark.udf.register("Method", (input: String) => { Sentiment140.as(input) })
                                     result = Some(data.withColumn("analysis", sentiment140(data.col("sentence"))))
      
      case "SentiStrength"        => import adapters.SentiStrength
                                     val sentiStrength = spark.udf.register("Method", (input: String) => { SentiStrength.as(input) })
                                     result = Some(data.withColumn("analysis", sentiStrength(data.col("sentence"))))

      case "SentiWordNet"         => import adapters.SentiWordNet
                                     val sentiWordNet = spark.udf.register("Method", (input: String) => { SentiWordNet.as(input) })
                                     result = Some(data.withColumn("analysis", sentiWordNet(data.col("sentence"))))

      case "SoCal"                => import adapters.SoCal
                                     val soCal = spark.udf.register("Method", (input: String) => { SoCal.as(input) })
                                     result = Some(data.withColumn("analysis", soCal(data.col("sentence"))))
      
      case "Stanford"             => import adapters.Stanford
                                     val stanford = spark.udf.register("Method", (input: String) => { Stanford.as(input) })
                                     result = Some(data.withColumn("analysis", stanford(data.col("sentence"))))

      case "Umigon"               => import adapters.Umigon
                                     val umigon = spark.udf.register("Method", (input: String) => { Umigon.as(input) })
                                     result = Some(data.withColumn("analysis", umigon(data.col("sentence"))))
      
      case "Vader"                => import adapters.Vader
                                     val vader = spark.udf.register("Method", (input: String) => { Vader.as(input) })
                                     result = Some(data.withColumn("analysis", vader(data.col("sentence"))))
      
      // DATABRICKS STANFORD CORENLP ===================================================================================================                              
      case "cleanxml"             => import com.databricks.spark.corenlp.functions._
                                     result = Some(data.select('*, cleanxml('sentence).as('analysis)))                 // clean xml tags
      case "ssplit"               => import com.databricks.spark.corenlp.functions._
                                     result = Some(data.select('*, explode(ssplit('sentence)).as('analysis)))          // text divider into sentences
      case "pos"                  => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, ('sentence), explode(ssplit('sentence)).as('sentences))       // Parts of Speech analysis
                                                          .select('*, ('sentence), pos('sentences).as('pos))
                                                          .select('*, ('sentence), stringify('pos).as('analysis))
                                                     )
      case "lemma"                => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, explode(ssplit('sentence)).as('sentences))       // generates lexemes from a sentence
                                                          .select('*, lemma('sentences).as('lexemas))
                                                          .select('*, stringify('lexemas).as('analysis))
                                                     )
      case "ner"                  => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, explode(ssplit('sentence)).as('sentences))       // generates named entities recognition
                                                          .select('*, ner('sentences).as('nerTags))
                                                          .select('*, stringify('nerTags).as('analysis))
                                                     )                                                                                     
      case "depparse"             => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, explode(ssplit('sentence)).as('sentences))       // generates semantic sentence dependencies
                                                          .select('*, depparse('sentences).as('depparsed))
                                                          .select('*, stringify2('depparsed).as('analysis))
                                                     )                                                  
      case "coref"                => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, coref('sentence).as('coref))                     // generates corref chain in documents
                                                          .select('*, stringify2('coref).as('analysis))
                                                     )
      case "natlog"               => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, explode(ssplit('sentence)).as('sentences))       // generates the natural logic notion o each token in sentence (p, down, flat)
                                                          .select('*, natlog('sentences).as('natlog))             
                                                          .select('*, stringify('natlog).as('analysis))
                                                     )  
      case "openie"               => import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, openie('sentence).as('openie))                   // generates a list of triples Open IE (subject, relation, target, confidence)
                                                          .select('*, stringify('openie).as('analysis))
                                                     )      
      case "db_stanford"          =>                                   
                                     import com.databricks.spark.corenlp.functions._
                                     result = Some(
                                                      data.select('*, sentiment('sentence).as('stanford_analysis))     //sentiment analysis between 0 and 4 (- <> +)
                                                          .select('*, when($"stanford_analysis" === 0, -1)             //adptation for -1, 0 and 1
                                                                 .when($"stanford_analysis" === 1, -1)
                                                                 .when($"stanford_analysis" === 2, 0)
                                                                 .when($"stanford_analysis" === 3, 1)
                                                                 .when($"stanford_analysis" === 4, 1)
                                                                 .as('analysis))
                                                     )                            
      // SPARK SENTIMENT ANALYSIS ====================================================================================================
      case "spark_naive_bayes"    => import adapters.Spark_NaiveBayes
                                     result = Some(
                                                      Spark_NaiveBayes.as(data)
                                                      .select('*, when($"prediction" === 0.0, -1)
                                                             .when($"prediction" === 1.0, 0)
                                                             .when($"prediction" === 2.0, 1)
                                                             .as('analysis))
                                                     )
                                     
      case "spark_random_forest" => import adapters.Spark_RandomForest
                                    result = Some(
                                                     Spark_RandomForest.as(data)
                                                     .select('*, when($"prediction" === 0.0, -1)
                                                            .when($"prediction" === 1.0, 0)
                                                            .when($"prediction" === 2.0, 1)
                                                            .as('analysis))
                                                     )
                                                     
      // (CLU)Lab STANFORD CORENLP ===================================================================================================
      /*case "clulab_stanford_processor" => import adapters.CLULabStanfordProcessor 
                                     val clulab_stanford_processor = spark.udf.register("Metodo", (input: String) => { CLULabStanfordProcessor.as(input) })
                                     resultado = Some(dados.withColumn("analysis", clulab_stanford_processor(dados.col("sentenca"))))
      
      case "clulab_fast_processor"=> import adapters.CLULabFastProcessor
                                     val clulab_fast_processor = spark.udf.register("Metodo", (input: String) => { CLULabFastProcessor.as(input) })
                                     resultado = Some(dados.withColumn("analysis", clulab_fast_processor(dados.col("sentenca"))))*/
      
      // COMPOSITION OF METHODS ======================================================================================================
      case "Composition"          => if (args.length < 4) {
                                         println("Invalid parameters to run the composition application!")
                                         println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Composition Afinn,Emolex 0.2,0.8")
                                         System.exit(1)
                                     }
                                     import adapters.Composition
                                     param_methods = args(2)
                                     param_weights = args(3)
                                     result = Some(Composition.as(param_methods, data, param_weights, spark))   
                                     //debug
                                     //result.get.show()
                                     //System.exit(1)
                                                     
      case _                      => println("****** Invalid option: "+method_parameter+" ******")
                                     System.exit(1)
    }                                          
    
    // OUTPUT ===========================================================================================================================
    //import java.io._
    val output = input + "." + method_parameter + ".output"
    
    //result.get.show()          //DEBUG
    
    if (method_parameter == "Composition") { 
      result.get                   //.get to remove Some of Option[DataFrame]
        .na.drop()                    //ignore regs with null
        //.rdd.saveAsTextFile(saida)  //alternative to save the result
        .write
        .mode("overwrite")
        .format("csv")
        .option("delimiter", "\t")
        .option("header", "true")
        .save(output)  
    } else {
      result.get                   //.get to remove Some of Option[DataFrame]
        .select("sentence", "analysis")
        .na.drop()                    //ignore regs with null
        //.rdd.saveAsTextFile(saida)  //alternative to save the result
        .write
        .mode("overwrite")
        .format("csv")
        .option("delimiter", "\t")
        .save(output)
    }
      
    println()
    println()
    println("-------------------------------------------------------------------")
    println(method_parameter + " finished successfully!")
    println("-------------------------------------------------------------------")
    println()
    println()
    
    spark.stop()
  }
}