package api

import adapters._
import com.databricks.spark.corenlp.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

object BigFeelAPI {
    
  def afinn(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Afinn.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def emolex(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Emolex.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def emoticons(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Emoticons.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def emoticonDS(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { EmoticonDS.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def happinessIndex(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { HappinessIndex.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def mpqa(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { MPQA.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def nrc(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { NRC.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def opinion(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Opinion.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def panasT(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { PanasT.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def sann(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Sann.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def sasa(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Sasa.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def senticNet(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { SenticNet.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def sentiment140(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Sentiment140.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def sentiStrength(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { SentiStrength.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def soCal(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { SoCal.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def stanford(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Stanford.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def umigon(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Umigon.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def vader(spark: SparkSession, data: DataFrame): DataFrame = {
    val metodo = spark.udf.register("Metodo", (input: String) => { Vader.as(input) })
    return data.withColumn("analysis", metodo(data.col("sentence")))
  }
  
  def db_stanford(spark: SparkSession, data: DataFrame): DataFrame = {
    return data.withColumn("analysis", sentiment(data.col("sentence")))
  }
  
  def spark_naive_bayes(spark: SparkSession, data: DataFrame): DataFrame = {
    import spark.implicits._                   // to use implicit column names
    import org.apache.spark.sql.functions._    // to use when function
    return Spark_NaiveBayes.as(data)
             .select(when($"prediction" === 0.0, -1)
                    .when($"prediction" === 1.0, 0)
                    .when($"prediction" === 2.0, 1)
                    .as('analysis))
  }
  
  def spark_random_forest(spark: SparkSession, data: DataFrame): DataFrame = {
    import spark.implicits._                   // to use implicit column names
    import org.apache.spark.sql.functions._    // to use when function
    return Spark_RandomForest.as(data)
             .select(when($"prediction" === 0.0, -1)
                    .when($"prediction" === 1.0, 0)
                    .when($"prediction" === 2.0, 1)
                    .as('analysis))
  }
  
  def composition(spark: SparkSession, data: DataFrame, methods: String, weights: String): DataFrame = {
    if (methods.length <= 0 || weights.length <= 0) {
        println("Invalid parameters to run the composition application!")
        println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Composition Afinn,Emolex 0.2,0.8")
        System.exit(1)
    }
    
    import adapters.Composition
    return Some(Composition.as(methods, data, weights, spark)).get
  }
  
  //...
}