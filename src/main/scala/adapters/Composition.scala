//Parameters to run BigFeel: data/teste.txt Composition Afinn,Emolex,Vader 0.1,0.5,0.4

package adapters

import org.apache.spark.sql.DataFrame
import org.apache.spark.ml._
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import scala.math.BigDecimal

//Afinn
class AfinnTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, AfinnTransformer] {
  def this() = this(Identifiable.randomUID("afinn"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Afinn
    Afinn.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Emolex
class EmolexTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, EmolexTransformer] {
  def this() = this(Identifiable.randomUID("emolex"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Emolex
    Emolex.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Emoticons
class EmoticonsTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, EmoticonsTransformer] {
  def this() = this(Identifiable.randomUID("emoticons"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Emoticons
    Emoticons.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//EmoticonDS
class EmoticonDSTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, EmoticonDSTransformer] {
  def this() = this(Identifiable.randomUID("emoticonDS"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.EmoticonDS
    EmoticonDS.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//HappinessIndex
class HappinessIndexTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, HappinessIndexTransformer] {
  def this() = this(Identifiable.randomUID("happinessIndex"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.HappinessIndex
    HappinessIndex.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//MPQA
class MPQATransformer(override val uid: String)
    extends UnaryTransformer[String, Int, MPQATransformer] {
  def this() = this(Identifiable.randomUID("mpqa"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.MPQA
    MPQA.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//NRCHashtag
class NRCHashtagTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, NRCHashtagTransformer] {
  def this() = this(Identifiable.randomUID("nrc"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.NRC
    NRC.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//OpinionLexicon
class OpinionLexiconTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, NRCHashtagTransformer] {
  def this() = this(Identifiable.randomUID("opinionLexicon"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Opinion
    Opinion.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//PanasT
class PanasTTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, PanasTTransformer] {
  def this() = this(Identifiable.randomUID("panasT"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.PanasT
    PanasT.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Sann
class SannTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SannTransformer] {
  def this() = this(Identifiable.randomUID("sann"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Sann
    Sann.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Sasa
class SasaTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SasaTransformer] {
  def this() = this(Identifiable.randomUID("sasa"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Sasa
    Sasa.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//SenticNet
class SenticNetTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SannTransformer] {
  def this() = this(Identifiable.randomUID("senticNet"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.SenticNet
    SenticNet.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Sentiment140
class Sentiment140Transformer(override val uid: String)
    extends UnaryTransformer[String, Int, Sentiment140Transformer] {
  def this() = this(Identifiable.randomUID("sentiment140"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Sentiment140
    Sentiment140.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//SentiStrength
class SentiStrengthTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SentiStrengthTransformer] {
  def this() = this(Identifiable.randomUID("sentiStrength"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.SentiStrength
    SentiStrength.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//SentiWordNet
class SentiWordNetTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SentiWordNetTransformer] {
  def this() = this(Identifiable.randomUID("sentiwordnet"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.SentiWordNet
    SentiWordNet.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//SoCal
class SoCalTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, SoCalTransformer] {
  def this() = this(Identifiable.randomUID("socal"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.SoCal
    SoCal.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Stanford
class StanfordTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, StanfordTransformer] {
  def this() = this(Identifiable.randomUID("stanford"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Stanford
    Stanford.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Umigon
class UmigonTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, UmigonTransformer] {
  def this() = this(Identifiable.randomUID("umigon"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Umigon
    Umigon.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Vader
class VaderTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, VaderTransformer] {
  def this() = this(Identifiable.randomUID("sann"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Vader
    Vader.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}

//Naive Bayes
/*
class Spark_NaiveBayesTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, Spark_NaiveBayesTransformer] {
  def this() = this(Identifiable.randomUID("spark_naive_bayes"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import adapters.Spark_NaiveBayes
    Spark_NaiveBayes.as(_)
  }
  protected def outputDataType: DataType = IntegerType
}
*/

//db_stanford
/*
class DbStanfordTransformer(override val uid: String)
    extends UnaryTransformer[String, Int, DbStanfordTransformer] {
  def this() = this(Identifiable.randomUID("db_stanford"))
  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => Int = {
    import com.databricks.spark.corenlp.functions._
    sentiment(_)
  }
  protected def outputDataType: DataType = IntegerType
}
*/

//Dummy UnaryTransformer
class DummyTransformer(override val uid: String)
    extends UnaryTransformer[String, String, DummyTransformer] {

  def this() = this(Identifiable.randomUID("dummy"))

  override protected def validateInputType(inputType: DataType): Unit = {
    require(inputType == StringType)
  }
  protected def createTransformFunc: String => String = {
    _.trim()
  }
  protected def outputDataType: DataType = StringType
}

// method for final result calculation
object FinalResult {
  import org.apache.spark.sql._
  def as(row: Row, methods_weights: Map[String, String]): Int = {
    
    var POSITIVE = row.getDecimal(row.fieldIndex("POSITIVE")).doubleValue()
    var NEUTRAL = row.getDecimal(row.fieldIndex("NEUTRAL")).doubleValue()
    var NEGATIVE = row.getDecimal(row.fieldIndex("NEGATIVE")).doubleValue()
    
    //possible cases in the POSITIVE, NEUTRAL and NEGATIVE columns:
    if ((POSITIVE > NEUTRAL) && (POSITIVE > NEGATIVE)) { return 1 }
    if ((NEGATIVE > POSITIVE) && (NEGATIVE > NEUTRAL)) { return -1 }
    if ((NEUTRAL > POSITIVE) && (NEUTRAL > NEGATIVE)) { return 0 }
    
    // if occurs a tie =========================================================================
    // get the highest weight
    import scala.collection.immutable.ListMap
    var orderedDescendingWeights = ListMap(methods_weights.toSeq.sortWith(_._2 > _._2):_*)    // descending order list of methods
    var (max_method, max_weight) = orderedDescendingWeights.head                              // gets the weighiest method (name, weight)
    if ((POSITIVE == NEUTRAL) && (POSITIVE == NEGATIVE)) {                                    // if it's a tie with the three columns, return the highest weight 
      return row.getInt(row.fieldIndex(max_method)) 
    }
    
    if ((POSITIVE == NEUTRAL) && (POSITIVE != NEGATIVE)) {                                    // the final results is between 0 and 1
      import scala.collection.mutable.Map
      var met_wei = Map[String,String]()                                                      // initializes an empty map of involved methods
      for ((met, wei) <- methods_weights) {                                                   // search for methods with 0 or 1 results
        if ((row.getInt(row.fieldIndex(met)) == 0) || (row.getInt(row.fieldIndex(met)) == 1)) {
          met_wei += (met -> wei)
        }
      }
      
      orderedDescendingWeights = ListMap(met_wei.toSeq.sortWith(_._2 > _._2):_*)              // descending order list of methods involved
      var (max_method, max_weight) = orderedDescendingWeights.head                            // gets the weighiest method (name, weight) 
      var count = 0
      for ((met, wei) <- met_wei) {                                                           // count the methods with the same max value of weight
        if (wei == max_weight) {
          count += 1
        }
      }
      
      if (count > 1) {                                                                        // if exists a tie between various weight methods, pick the first one                                                                      
        for ((met, wei) <- methods_weights) {                                                 // search for the first method with a max_weight
          if (wei == max_weight) {
            return row.getInt(row.fieldIndex(met))                                            // returns the first one
          }
        }  
      } else {
        return return row.getInt(row.fieldIndex(max_method))                                  // returns the max_method, after all, he is the only with max_weight
      }
 
      return 9  //indicates error
    }
    
    if ((POSITIVE == NEGATIVE) && (POSITIVE != NEUTRAL)) { return 0 }                         // if the final result is between positive and negative, so the result is neutral
    
    if ((NEUTRAL == NEGATIVE) && (NEUTRAL != POSITIVE)) {                                     // the final results is between -1 and 0 
      import scala.collection.mutable.Map
      var met_wei = Map[String,String]()                                                      // initializes an empty map of involved methods
      for ((met, wei) <- methods_weights) {                                                   // search for methods with 0 or 1 results
        if ((row.getInt(row.fieldIndex(met)) == -1) || (row.getInt(row.fieldIndex(met)) == 0)) {
          met_wei += (met -> wei)
        }
      }
      
      orderedDescendingWeights = ListMap(met_wei.toSeq.sortWith(_._2 > _._2):_*)              // descending order list of methods involved
      var (max_method, max_weight) = orderedDescendingWeights.head                            // gets the weighiest method (name, weight) 
      var count = 0
      for ((met, wei) <- met_wei) {                                                           // count the methods with the same max value of weight
        if (wei == max_weight) {
          count += 1
        }
      }
      
      if (count > 1) {                                                                        // if exists a tie between various weight methods, pick the first one                                                                      
        for ((met, wei) <- methods_weights) {                                                 // search for the first method with a max_weight
          if (wei == max_weight) {
            return row.getInt(row.fieldIndex(met))                                            // returns the first one
          }
        }  
      } else {
        return return row.getInt(row.fieldIndex(max_method))                                  // returns the max_method, after all, he is the only with max_weight
      }
 
      return 9  //indicates error
    }
    
    return 9  //indicates error
  }
}

//Composition Method
object Composition {
  
  def as(methods: String, data: DataFrame, weights: String, spark: SparkSession): DataFrame = {
    
    var res = data
    val methods_list = methods.split(",")
    val weights_list = weights.split(",")
    val last_method = methods_list.last
    
    //validates de lists of methods and weights
    if (methods_list.length != weights_list.length) {
        println("Invalid parameters to run the composition application! The number of methods needs to be the same as the number of weights!")
        println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Composition Afinn,Emolex 0.2,0.8")
        System.exit(1)
    }
    
    var sum = 0.0 
    for (w <- weights_list) {
      sum = sum + w.toFloat 
    }
    sum = BigDecimal(sum).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
    
    //validates the sum of weights
    if (sum != 1.0) {
        println("The sum of weights must be exactly 1.0!")
        println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Composition Afinn,Emolex 0.2,0.8")
        System.exit(1)
    }
    
    //validates if some weight is equal to zero
    for (w <- weights_list) {
      var testW = 0.0
      testW = BigDecimal(w.toFloat).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
      if (testW == 0.0) {
        println("The weights must be greater then 0.0!")
        println("Example: spark-submit --class app.BigFeel --master spark://10.120.1.50:7077 bigfeel.jar hdfs://master:9000/test.txt Composition Afinn,Emolex 0.2,0.8")
        System.exit(1)
      }
    }

    val methods_weights = (methods_list zip weights_list).toMap
    /*import scala.collection.immutable.ListMap
    val orderedDescendingW = ListMap(methods_weights.toSeq.sortWith(_._2 > _._2):_*)    //descending order
*/    
    //debug
    /*println(methods_list)
    println(weights_list)
    methods_weight.foreach(x => println (x._1 + "-->" + x._2))
    for (m <- methods_list) {
      println(m)
    }
    System.exit(1)*/
    
    if (methods contains "Afinn") {
      res = new AfinnTransformer().setInputCol("sentence").setOutputCol("Afinn").transform(res)
    } 
    
    if (methods contains "Emolex") {
      res = new EmolexTransformer().setInputCol("sentence").setOutputCol("Emolex").transform(res)
    }
    
    if (methods contains "Emoticons") {
      res = new EmoticonsTransformer().setInputCol("sentence").setOutputCol("Emoticons").transform(res)
    }
    
    if (methods contains "EmoticonDS") {
      res = new EmoticonDSTransformer().setInputCol("sentence").setOutputCol("EmoticonDS").transform(res)
    }
    
    if (methods contains "HappinessIndex") {
      res = new HappinessIndexTransformer().setInputCol("sentence").setOutputCol("HappinessIndex").transform(res)
    }
    
    if (methods contains "MPQA") {
      res = new MPQATransformer().setInputCol("sentence").setOutputCol("MPQA").transform(res)
    }
    
    if (methods contains "NRCHashtag") {
      res = new NRCHashtagTransformer().setInputCol("sentence").setOutputCol("NRCHashtag").transform(res)
    }
    
    if (methods contains "OpinionLexicon") {
      res = new OpinionLexiconTransformer().setInputCol("sentence").setOutputCol("OpinionLexicon").transform(res)
    }
    
    if (methods contains "PanasT") {
      res = new PanasTTransformer().setInputCol("sentence").setOutputCol("PanasT").transform(res)
    }
    
    if (methods contains "Sann") {
      res = new SannTransformer().setInputCol("sentence").setOutputCol("Sann").transform(res)
    }
    
    if (methods contains "Sasa") {
      res = new SasaTransformer().setInputCol("sentence").setOutputCol("Sasa").transform(res)
    }
    
    if (methods contains "SenticNet") {
      res = new SenticNetTransformer().setInputCol("sentence").setOutputCol("SenticNet").transform(res)
    }
    
    if (methods contains "Sentiment140") {
      res = new Sentiment140Transformer().setInputCol("sentence").setOutputCol("Sentiment140").transform(res)
    }
    
    if (methods contains "SentiStrength") {
      res = new SentiStrengthTransformer().setInputCol("sentence").setOutputCol("SentiStrength").transform(res)
    }
    
    if (methods contains "SentiWordNet") {
      res = new SentiWordNetTransformer().setInputCol("sentence").setOutputCol("SentiWordNet").transform(res)
    }
    
    if (methods contains "SoCal") {
      res = new SoCalTransformer().setInputCol("sentence").setOutputCol("SoCal").transform(res)
    }
    
    if (methods contains "Stanford") {
      res = new StanfordTransformer().setInputCol("sentence").setOutputCol("Stanford").transform(res)
    }
    
    if (methods contains "Umigon") {
      res = new UmigonTransformer().setInputCol("sentence").setOutputCol("Umigon").transform(res)
    }
    
    if (methods contains "Vader") {
      res = new VaderTransformer().setInputCol("sentence").setOutputCol("Vader").transform(res)
    }
    
    if (methods contains "db_stanford") {
      import spark.implicits._
      import org.apache.spark.sql.functions._
      import com.databricks.spark.corenlp.functions._
      
      res = res.select('*, sentiment('sentence).as('stanford_analysis))
               .select('*, when($"stanford_analysis" === 0, -1)             //adptation for -1, 0 and 1
                      .when($"stanford_analysis" === 1, -1)
                      .when($"stanford_analysis" === 2, 0)
                      .when($"stanford_analysis" === 3, 1)
                      .when($"stanford_analysis" === 4, 1)
                      .as('db_stanford))
    }
    
    if (methods contains "spark_naive_bayes") {
      import spark.implicits._
      import org.apache.spark.sql.functions._
      
      res = Spark_NaiveBayes.as(res)
                            .select('*, when($"prediction" === 0.0, -1)
                                   .when($"prediction" === 1.0, 0)
                                   .when($"prediction" === 2.0, 1)
                                   .as('spark_naive_bayes)
                                   )
    }
    
    if (methods contains "spark_random_forest") {
      import spark.implicits._
      import org.apache.spark.sql.functions._
      
      res = Spark_RandomForest.as(res)
                            .select('*, when($"prediction" === 0.0, -1)
                                   .when($"prediction" === 1.0, 0)
                                   .when($"prediction" === 2.0, 1)
                                   .as('spark_random_forest)
                                   )
    }
    
    //res.show()      //DEBUG
    //System.exit(0)  //DEBUG
    
    //val method_results = res
    import org.apache.spark.sql.functions._
    
    // Register the DataFrame as a SQL temporary view
    res.createOrReplaceTempView("results")
    
    //Prepare the sql string for consulting    
    var sql = "SELECT *, "
    //POS
    for ((met, wei) <- methods_weights) {
      sql += "(CASE "+ met +" WHEN 1 THEN "+ wei +" ELSE 0.0 END) AS "+met+"Pos"
      if (met != last_method) {
        sql += ","
      }
    }
    sql += ","
    //NEU
    for ((met, wei) <- methods_weights) {
      sql += "(CASE "+ met +" WHEN 0 THEN "+ wei +" ELSE 0.0 END) AS "+met+"Neu"
      if (met != last_method) {
        sql += ","
      }
    }
    sql += ","
    //NEG
    for ((met, wei) <- methods_weights) {
      sql += "(CASE "+ met +" WHEN -1 THEN "+ wei +" ELSE 0.0 END) AS "+met+"Neg"
      if (met != last_method) {
        sql += ","
      }
    }
    sql += " FROM results"
    
    res = spark.sql(sql)
    //debug
    /*res.show()
    System.exit(1)*/
    
    res.createOrReplaceTempView("results")
    
    //Prepare the sql string for summing 
    sql = "SELECT *,"
    //POSITIVE
    sql += "("
    for ((met, wei) <- methods_weights) {
      sql += met +"Pos"
      if (met != last_method) {
        sql += "+"
      }
    }
    sql += ") AS POSITIVE,"
    //NEUTRAL
    sql += "("
    for ((met, wei) <- methods_weights) {
      sql += met +"Neu"
      if (met != last_method) {
        sql += "+"
      }
    }
    sql += ") AS NEUTRAL,"
    //NEGATIVE
    sql += "("
    for ((met, wei) <- methods_weights) {
      sql += met +"Neg"
      if (met != last_method) {
        sql += "+"
      }
    }
    sql += ") AS NEGATIVE"
    sql += " FROM results"

                    /*(AfinnPos + EmolexPos + VaderPos) AS POSITIVE,
                    (AfinnNeu + EmolexNeu + VaderNeu) AS NEUTRAL,
                    (AfinnNeg + EmolexNeg + VaderNeg) AS NEGATIVE
                		FROM results*/
    
    res = spark.sql(sql)
    //debug
    /*res.show()
    System.exit(1)*/
    import org.apache.spark.sql._                                                                              // import Row
    val final_result = spark.udf.register("Method", (row: Row) => { FinalResult.as(row, methods_weights) })    // register the udf
    res = res.withColumn("analysis", final_result(struct(res.columns.map(res(_)) : _*)))                       // call udf sending the entire row with n columns
    
    /*res.createOrReplaceTempView("results")
    val (key, value) = orderedDescendingW.head  // the name of column with the maximum weight
    //10 = max_weight_column
    //9  = positive = neutral (greatest(involved_methods))
    //8  = positive = negative (greatest(involved_methods)) = neutral
    //7  = neutral = negative (greatest(involved_methods))
    sql = "SELECT *, "
    sql += "          (CASE "
    sql += "              WHEN ((POSITIVE > NEUTRAL) AND (POSITIVE > NEGATIVE)) THEN 1 "
    sql += "                      WHEN ((NEGATIVE > POSITIVE) AND (NEGATIVE > NEUTRAL)) THEN -1 "
    sql += "                      WHEN ((NEUTRAL > POSITIVE) AND (NEUTRAL > NEGATIVE)) THEN 0 "
    sql += "                      ELSE (CASE "
    sql += "                  			      WHEN (POSITIVE = NEUTRAL AND POSITIVE = NEGATIVE) THEN "+ key +" "
    sql += "                              WHEN (POSITIVE = NEUTRAL AND POSITIVE <> NEGATIVE) THEN '0|1' "
    sql += "                              WHEN (POSITIVE = NEGATIVE AND POSITIVE <> NEUTRAL) THEN 0 "
    sql += "                              WHEN (NEUTRAL = NEGATIVE AND NEUTRAL <> POSITIVE) THEN '-1|0' "
    sql += "                  		  END) "
    sql += "                  END) AS analysis"
    sql += "                  FROM results"
    
    res = spark.sql(sql)
    //debug*/
    //res.show()
    //res.printSchema()
    //System.exit(1)
    
    /*//Methods that are responsible for summary polarity results
    res.createOrReplaceTempView("results")
    sql = """SET @resp_positive = ''"""
    res = spark.sql(sql)
    
    sql = """SELECT *,
                      (CASE
                          WHEN (Afinn = 1) THEN @resp_positive := CONCAT(@resp_positive, 'Afinn')
                          WHEN (Emolex = 1) THEN @resp_positive := CONCAT(@resp_positive,'Emolex')
                          WHEN (Vader = 1) THEN @resp_positive := CONCAT(@resp_positive, 'Vader')
                      END) AS @resp_positive
                      FROM results"""
    
    res = spark.sql(sql)
    //debug
    res.show()
    System.exit(1)*/
    
    //assembly the final composition analysis (handling a tie situation)
    /*res.createOrReplaceTempView("results")
    
    val (key, value) = orderedDescendingW.head  // the name of column with the maximum weight
    
    sql = "SELECT *,"
    sql += "(CASE "
      sql += "WHEN (pre_analysis = 1) THEN 1 "
      sql += "WHEN (pre_analysis = 0) THEN 0 "
      sql += "WHEN (pre_analysis = -1) THEN -1 "
      sql += "WHEN (pre_analysis = 10) THEN " + key + " "
      sql += "WHEN (pre_analysis = 9) THEN 1|0 "
      sql += "WHEN (pre_analysis = 8) THEN 8 "
      sql += "WHEN (pre_analysis = 7) THEN 7 "
    sql += "END) AS analysis "
    sql += "FROM results"
                      			
    res = spark.sql(sql)
    res.show()
    System.exit(1)*/
    
    //final filter
    res.createOrReplaceTempView("results")
    
    sql = "SELECT sentence,"
    for (met  <- methods_list) {
      sql += met + ","
    }
    sql += "POSITIVE,NEUTRAL,NEGATIVE,analysis FROM results"
    
    res = spark.sql(sql)
    return res
    
    //debug
    //res.show()
    //System.exit(1)
    
    //The error comment from https://www.mail-archive.com/issues@spark.apache.org/msg186357.html
    
    /*> GeneratedIterator grows beyond 64 KB
    
    This is an open source project. You cannot demand that anyone 
    "solve this asap". People are contributing their free time or time donated by 
    the companies that employ them.
    
    If you want someone to fix this for you "asap", perhaps you should look at paid 
    support from Databricks, Cloudera, Hortonworks, Amazon, or some other big 
    company that works with Spark.*/
  }
}