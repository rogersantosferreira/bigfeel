package adapters

import java.util.ArrayList
import com.lg.sentiment.Method
import com.lg.sentiment.MethodCreator
import java.net.URL

object Afinn {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val afinn = creator.createMethod(1)
  def as(s: String): Int = {
    return afinn.analyseText(s)
  }
}

object Emolex {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val emolex = creator.createMethod(2)
  def as(s: String): Int = {
    return emolex.analyseText(s)
  }
}

object Emoticons {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val emoticons = creator.createMethod(3)
  def as(s: String): Int = {
    return emoticons.analyseText(s)
  }
}

object EmoticonDS {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val emoticonDS = creator.createMethod(4)
  def as(s: String): Int = {
    return emoticonDS.analyseText(s)
  }
}

object HappinessIndex {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val happinessIndex = creator.createMethod(5)
  def as(s: String): Int = {
    return happinessIndex.analyseText(s)
  }
}

object MPQA {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val mpqa = creator.createMethod(6)
  def as(s: String): Int = {
    return mpqa.analyseText(s)
  }
}

object NRC {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  
  val nrc = creator.createMethod(7)
  def as(s: String): Int = {
    return nrc.analyseText(s)
  }
}

object Opinion {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val opinion = creator.createMethod(8)
  def as(s: String): Int = {
    return opinion.analyseText(s)
  }
}

object PanasT {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val panasT = creator.createMethod(9)
  def as(s: String): Int = {
    return panasT.analyseText(s)
  }
}

object Sann {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val sann = creator.createMethod(10)
  def as(s: String): Int = {
    return sann.analyseText(s)
  }
}

object Sasa {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val sasa = creator.createMethod(11)
  def as(s: String): Int = {
    return sasa.analyseText(s)
  }
}

object SenticNet {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val senticNet = creator.createMethod(12)
  def as(s: String): Int = {
    return senticNet.analyseText(s)
  }
}

object Sentiment140 {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val sentiment140 = creator.createMethod(13)
  def as(s: String): Int = {
    return sentiment140.analyseText(s)
  }
}

object SentiStrength {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val sentiStrength = creator.createMethod(14)
  def as(s: String): Int = {
    return sentiStrength.analyseText(s)
  }
}

object SentiWordNet {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val sentiWordNet = creator.createMethod(15)
  def as(s: String): Int = {
    return sentiWordNet.analyseText(s)
  }
}

object SoCal {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance() 
  val soCal = creator.createMethod(16)
  def as(s: String): Int = {
    return soCal.analyseText(s)
  }
}

object Stanford {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val stanford = creator.createMethod(17)
  def as(s: String): Int = {
    return stanford.analyseText(s)
  }
}

object Umigon {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()  
  val umigon = creator.createMethod(18)
  def as(s: String): Int = {
    return umigon.analyseText(s)
  }
}

object Vader {
  MethodCreator.resourcePath = "resources"
  var creator = MethodCreator.getInstance()
  val vader = creator.createMethod(19)
  def as(s: String): Int = {
    return vader.analyseText(s)
  }
}