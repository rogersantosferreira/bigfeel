package adapters

    // This is an example of a multi-classes classifier using Naive Bayes
    // SOURCE OF DATASET: https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html
    // ===========================================================================================================
    // This is the way we can save the trainned model
    //  SAVING:
    //    modelo.save("modelo")
    //  LOADING (via Scala REPL console (Read-Evaluate-Print Loop)):
    //    import org.apache.spark.ml.PipelineModel
    //    val modelo_treinado = PipelineModel.load("modelo")
    //  USING:
    //    val data2 = spark.read.format("libsvm").option("header", "false").option("inferSchema", "true").load("news20.full")
    //    modelo_treinado.transform(data2)
    //
    //  Example: step-by-step Naive Bayes with hashing of features from text-file


  import org.apache.spark.sql.SparkSession
  import org.apache.spark.ml.Pipeline
  import org.apache.spark.ml.classification.NaiveBayes
  import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
  import org.apache.spark.ml.feature.{IndexToString, StringIndexer, VectorIndexer}
  import org.apache.log4j._
  
  import org.apache.spark.ml.feature.{RegexTokenizer, Tokenizer}
  import org.apache.spark.sql.functions._
  import org.apache.spark.ml.feature.HashingTF
  import org.apache.spark.ml.feature.LabeledPoint
  
  object SparkML1 {
    
    def main(args: Array[String]): Unit = {
      Logger.getLogger("org").setLevel(Level.ERROR)
      
      // Spark Session
      val spark = (SparkSession.builder()
                   .master("local[*]")
                   .getOrCreate())
      
      // Loading the data for trainning and testing
      val data = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","|")
                 .option("inferSchema","true")
                 .load("data/finefoods10.csv").toDF("comentarios","rotulos"))
      
      val data2 = data.select("rotulos", "comentarios").toDF("label", "sentence")
      
      // CONVERTING TEXT TO LIBSVM FORMAT ========================================
      val tokenizer = new Tokenizer()
                      .setInputCol("sentence")
                      .setOutputCol("words")
      val regexTokenizer = new RegexTokenizer()
                           .setInputCol("sentence")
                           .setOutputCol("words")
      
      val tokenized = tokenizer.transform(data2)
      val data3 = tokenized.select("label", "words").toDF("label", "features")
      
      val hashingTF = new HashingTF()
                      .setInputCol("features").setOutputCol("rawFeatures")
      
      val featurizedData = hashingTF.transform(data3)
      
      val data4 = featurizedData.select("label", "rawFeatures").toDF("label", "features")
      // ================================================================================
      
      // Indexing the labels, adding metadata to labels column.
      // Changes the original dataset to the new indexed labels.
      val labelIndexer = (new StringIndexer()
                         .setInputCol("label")
                         .setOutputCol("indexedLabel")
                         .fit(data4))
                         
      // Automatically identifies categorical features, indexing them
      val featureIndexer = (new VectorIndexer()
                           .setInputCol("features")
                           .setOutputCol("indexedFeatures")
                           .setMaxCategories(10) // features with > 10 distinct values are treated as continuous.
                           .fit(data4))
      
      // Data splitting (70%: trainning - 30%: testing)
      val Array(treinamento, teste) = data4.randomSplit(Array(0.7, 0.3))
      
      // Trainning NaiveBayes model.
      val dt = (new NaiveBayes()
               .setLabelCol("indexedLabel")
               .setFeaturesCol("indexedFeatures"))
      
      // Converting the indexed labels to original value.
      val labelConverter = (new IndexToString()
              .setInputCol("prediction")
              .setOutputCol("predictedLabel")
              .setLabels(labelIndexer.labels))
      
      // Chain feature indexing with Naive Bayes classifier in a pipeline.
      val pipeline = (new Pipeline().setStages(Array(labelIndexer, featureIndexer, dt, labelConverter)))
      
      // Train the model, executing before the indexing methods.
      val modelo = pipeline.fit(treinamento)
      
      // Test the model making predictions.
      val predicoes = modelo.transform(teste)
      
      // Shows only the first 5 results as an example.
      predicoes.select("predictedLabel", "label", "features").show()
      
      // Selects (prediction, original label) and calcs the error value
      /*val evaluator = (new MulticlassClassificationEvaluator()
                      .setLabelCol("label")
                      .setPredictionCol("prediction")
                      .setMetricName("accuracy"))
      
      val accuracy = evaluator.evaluate(predicoes)
      println("Accuracy: " + accuracy)*/
    }
 }
 