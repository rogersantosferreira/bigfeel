package adapters

//disabled for incompatibility between the libraries that compound Stanford CoreNLP
/*import org.clulab.processors.{Document, Processor}
import org.clulab.processors.corenlp.CoreNLPProcessor
import org.clulab.processors.corenlp.CoreNLPSentimentAnalyzer
import org.clulab.processors.fastnlp.FastNLPProcessor*/
import org.apache.log4j.Level
import org.apache.log4j.Logger


object CLULabStanfordProcessor  {
  /*val processor:Processor = new CoreNLPProcessor()
  
  def as(s: String): Int = {
    val doc = processor.annotate(s)
    val sentenca = doc.sentences.head
    val score = CoreNLPSentimentAnalyzer.sentiment(sentenca)
    //println("\n\nRESULTADO = " + score + "\n\n")  
    score match {
      case 0 => return -1
      case 1 => return -1
      case 2 => return 0
      case 3 => return 1
      case 4 => return 1
      case _ => return score
    }
  }*/
}

object CLULabFastProcessor {
  /*val processor:Processor = new FastNLPProcessor()
  
  def as(s: String): Int = {
    val doc = processor.annotate(s)
    val sentenca = doc.sentences.head
    val score = CoreNLPSentimentAnalyzer.sentiment(sentenca)
    //println("\n\nRESULTADO = " + score + "\n\n")  
    score match {
      case 0 => return -1
      case 1 => return -1
      case 2 => return 0
      case 3 => return 1
      case 4 => return 1
      case _ => return score
    }
  }*/
}