package adapters

  // Example: Implementation of NaiveBayes and Random Forest classifiers in Spark (just for comparison with another methods)
  // IMPORTANT: To manage this method to work, it is necessary to copy the entire folder resources to each slave node on the cluster, exactly on the same path of this in master node.  

  import org.apache.spark.sql.SparkSession
  import org.apache.spark.ml.Pipeline
  import org.apache.spark.ml.classification.NaiveBayes
  import org.apache.log4j._
  
  import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, StopWordsRemover}
  import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
  import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
  import org.apache.spark.sql.functions._  
  import org.apache.spark.sql.DataFrame

  
  object Spark_NaiveBayes {
    
    def as(df: DataFrame): DataFrame = {
      // CONVERTING TEXTO TO LIBSVM FORMAT ========================================
      // Define tokenizer
      val tokenizer = new Tokenizer()
                      .setInputCol("sentence")
                      .setOutputCol("words")
                      
      // Define stopwords remover                
      val remover = new StopWordsRemover()
            				.setInputCol("words")
            				.setOutputCol("filtered")
      
      // Create and configure TF-IDF
      val numFeatures = 3000            //best value get by Crossvalidation method
		  val minDocFreq = 3                //best value get by Crossvalidation method
      
		  // TF
      val hashingTF = new HashingTF()
              				.setInputCol("filtered")
              				.setOutputCol("tf")
              				.setNumFeatures(numFeatures)
		  
      // IDF
		  val idf = new IDF()
        				.setInputCol("tf")
        				.setOutputCol("features")
        				.setMinDocFreq(minDocFreq)
		  
		  // Load instance of NaiveBayes classifier 
		  val nb = CrossValidatorModel.load("resources/modelNB")  //doesnt working on cluster
		  
		  val pipeline = new Pipeline()
				             .setStages(Array(tokenizer, remover, hashingTF, idf, nb))
		  
			//run the sentiment classification
			val predictions = nb.transform(df)
		  //predictions.show()
			return predictions
    }
  }
  
  object Spark_RandomForest {
    
    def as(df: DataFrame): DataFrame = {
      // CONVVERTING TEXT TO LIBSVM FORMAT ========================================
      // Define tokenizer
      val tokenizer = new Tokenizer()
                      .setInputCol("sentence")
                      .setOutputCol("words")
                      
      // Define the stopwords remover                
      val remover = new StopWordsRemover()
            				.setInputCol("words")
            				.setOutputCol("filtered")
      
      // Create and configure TF-IDF
      val numFeatures = 5000
		  val minDocFreq = 2
      
		  // TF
      val hashingTF = new HashingTF()
              				.setInputCol("filtered")
              				.setOutputCol("tf")
              				.setNumFeatures(numFeatures)
		  
      // IDF
		  val idf = new IDF()
        				.setInputCol("tf")
        				.setOutputCol("features")
        				.setMinDocFreq(minDocFreq)
		  
		  // Load instance of RandomForest Classifier
		  val rf = CrossValidatorModel.load("resources/modelRF")
		  
		  val pipeline = new Pipeline()
				             .setStages(Array(tokenizer, remover, hashingTF, idf, rf))
		  
			//run the sentiment classification
			val predictions = rf.transform(df)
		  //predictions.show()
			return predictions
    }
 }
 