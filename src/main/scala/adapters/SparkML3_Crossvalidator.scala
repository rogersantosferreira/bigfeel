package adapters

    // This is an example of a multi-classes classifier using Naive Bayes and Random Forest
    // SOURCE OF DATASET: https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html
    // ===========================================================================================================
    // This is the way we can save the trainned model
    //  SAVING:
    //    modelo.save("modelo")
    //  LOADING (via Scala REPL console (Read-Evaluate-Print Loop)):
    //    import org.apache.spark.ml.PipelineModel
    //    val modelo_treinado = PipelineModel.load("modelo")
    //  USING:
    //    val data2 = spark.read.format("libsvm").option("header", "false").option("inferSchema", "true").load("news20.full")
    //    modelo_treinado.transform(data2)
    //
    //  Example: Naive Bayes and Random Forest - trainning based on Cross-Validation example from Spark docs

  
  import org.apache.spark.sql.SparkSession
  import org.apache.spark.ml.Pipeline
  import org.apache.spark.ml.classification.NaiveBayes
  import org.apache.spark.ml.classification.LogisticRegression
  import org.apache.spark.ml.classification.{RandomForestClassificationModel, RandomForestClassifier}
  import org.apache.log4j._
  
  import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, StopWordsRemover}
  import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
  import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
  import org.apache.spark.sql.functions._  
  
  
  object SparkML3_Trainning {
    
    def main(args: Array[String]): Unit = {
      Logger.getLogger("org").setLevel(Level.ERROR)
      
      // Spark Session
      val spark = (SparkSession.builder()
                   .master("local[*]")
                   .getOrCreate())
      
      // Loading the data for trainning and testing
      val data2 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load("data/teste_3classes.csv").toDF("sentence","label"))
      
      val data = data2.na.drop()
                 
      // CONVERTING TEXT TO LIBSVM FORMAT ===========================================
      // Defines tokenizer
      val tokenizer = new Tokenizer()
                      .setInputCol("sentence")
                      .setOutputCol("words")
                      
      // Defines stop words remover                
      val remover = new StopWordsRemover()
            				.setInputCol("words")
            				.setOutputCol("filtered")
      
      // Creates and setup the TF-IDF
      val numFeatures = 5000
		  val minDocFreq = 2
      
		  // TF
      val hashingTF = new HashingTF()
              				.setInputCol("filtered")
              				.setOutputCol("tf")
              				.setNumFeatures(numFeatures)
		  
      // IDF
		  val idf = new IDF()
        				.setInputCol("tf")
        				.setOutputCol("features")
        				.setMinDocFreq(minDocFreq)
		  
		  // Creates the classifier instance
		  //val metodo = new NaiveBayes()
      //val metodo = new LogisticRegression()
      val metodo = new RandomForestClassifier()
		  
		  val pipeline = new Pipeline()
				             .setStages(Array(tokenizer, remover, hashingTF, idf, metodo))
		  
			// Creates and set of grid for tunning and search for the best values in the Cossvalidator.
			// To create another classifier/method you need to search the Spark API docs for know the params and they value interval.	             
		  val paramGrid = new ParamGridBuilder()
              				.addGrid(hashingTF.numFeatures, Array(10, 100, 1000, 2000, 3000, 4000, 5000))   // Number of TF features - for all methods. Do not remove
              				.addGrid(idf.minDocFreq, Array(1,2,3,4,5,6))                                    // Minimum frequency for IDF - for all methods. Do not remove
              				
              				//Naive Bayes param
              				//.addGrid(metodo.smoothing, Array(0.0, 0.2, 0.4, 0.6, 0.8, 1.0))                 
              				
              				//Logistic Regression params
              				//.addGrid(metodo.maxIter, Array(10, 20, 30, 40, 50))
              				//.addGrid(metodo.regParam, Array(0.0, 0.2, 0.4, 0.6, 0.8, 1.0))
              				//.addGrid(metodo.elasticNetParam, Array(0.0, 0.2, 0.4, 0.6, 0.8, 1.0))
              				
              				//Random Forest params
              				.addGrid(metodo.numTrees, Array(5, 10, 15, 20, 25, 30))
              				.addGrid(metodo.maxBins, Array(3, 9, 27, 32))
              				.addGrid(metodo.maxDepth, Array(2, 5, 10, 15))
              				
              				.build()
		  
		  // Executes the cross validation with 10 dynamic splitted folds
		  val cv = new CrossValidator()
      				 .setEstimator(pipeline)
      				 .setEvaluator(new MulticlassClassificationEvaluator())
      				 .setEstimatorParamMaps(paramGrid)
      				 .setNumFolds(10)
      				 
      try // If the model already exists, them load it
      {
        //val cvModel = CrossValidatorModel.load("modelNB")
        //val cvModel = CrossValidatorModel.load("modelLR")
        val cvModel = CrossValidatorModel.load("modelRF")
      } 
      catch
      { // If the model doesn't exists, so create a new model.
        case _: Throwable => println("Model not find! Creating a new one.")
			                       val cvModel = cv.fit(data)    // Train
			                       //cvModel.save("modelNB");      // Save the model
			                       //cvModel.save("modelLR");      // Save the model
			                       cvModel.save("modelRF");      // Save the model
      }
    }
 }
 