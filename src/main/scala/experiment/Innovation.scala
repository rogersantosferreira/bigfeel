package experiment

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.{RegexTokenizer, Tokenizer}
import com.databricks.spark.corenlp.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import scala.collection.JavaConversions._  //to transparent convert java.util.List to scala.List
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, StopWordsRemover}
import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.ml.classification.LinearSVC
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.feature.Word2Vec
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.feature.ChiSqSelector

object Execution {
  
  //declarations of global resources
  var op = "OR"
  
  var lex_syn: Option[DataFrame] = None
  
  val pos_rules: List[String] = List( /*//tight rules
                                      "PRP|MD|VBZ|VB|JJR","PRP|MD|VBZ|VB|JJ","PRP|MD|VBZ|VB|NN","PRP|MD|VBZ|VB|NNP","PRP|MD|VBZ|VB|NNS",
                                      "PRP|MD|VB|JJR","PRP|MD|VB|JJ","PRP|MD|VB|NN","PRP|MD|VB|NNP","PRP|MD|VB|NNS",
                                      "NNP|MD|VBZ|VB|JJR","NNP|MD|VBZ|VB|JJ","NNP|MD|VBZ|VB|NN","NNP|MD|VBZ|VB|NNP","NNP|MD|VBZ|VB|NNS",
                                      "NNP|MD|VB|JJR","NNP|MD|VB|JJ","NNP|MD|VB|NN","NNP|MD|VB|NNP","NNP|MD|VB|NNS",
                                      "NNS|MD|VBZ|VB|JJR","NNS|MD|VBZ|VB|JJ","NNS|MD|VBZ|VB|NN","NNS|MD|VBZ|VB|NNP","NNS|MD|VBZ|VB|NNS",
                                      "NNS|MD|VB|JJR","NNS|MD|VB|JJ","NNS|MD|VB|NN","NNS|MD|VB|NNP","NNS|MD|VB|NNS",
                                      "NN|MD|VBZ|VB|JJR","NN|MD|VBZ|VB|JJ","NN|MD|VBZ|VB|NN","NN|MD|VBZ|VB|NNP","NN|MD|VBZ|VB|NNS",
                                      "NN|MD|VB|JJR","NN|MD|VB|JJ","NN|MD|VB|NN","NN|MD|VB|NNP","NN|MD|VB|NNS",
                                      "MD|VB|JJR","MD|VB|JJ","MD|VB|NN","MD|VB|NNP","MD|VB|NNS",
                                      "MD|VBZ|VB|JJR","MD|VBZ|VB|JJ","MD|VBZ|VB|NN","MD|VBZ|VB|NNP","MD|VBZ|VB|NNS",
                                      
                                      //light rules
                                      "PRP|MD|VBZ","PRP|MD|VB",
                                      "NNP|MD|VBZ","NNP|WDT|MD|VBZ","NNP|MD|VB","NNP|WDT|MD|VB",
                                      "NNS|MD|VBZ","NNS|WDT|MD|VBZ","NNS|MD|VB","NNS|WDT|MD|VB",
                                      "NN|MD|VBZ","NN|WDT|MD|VBZ","NN|MD|VB","NN|WDT|MD|VB",
                                      "MD|VB|JJR","MD|VB|JJ","MD|VB|NN","MD|VB|NNP","MD|VB|NNS",
                                      "MD|VBZ|VB|JJR","MD|VBZ|VB|JJ","MD|VBZ|VB|NN","MD|VBZ|VB|NNP","MD|VBZ|VB|NNS"*/
                                      
                                      //analytical rules
                                      "NNS|MD|VB","NNP|MD|VBN","NN|MD|VB","NNP|MD|VB",
                                      "MD|VB","MD|VB|JJR","MD|VB|JJ","MD|VB|NN",
                                      "PRP|MD|NN|NN","PRP|MD|VBZ|VB"
                                     )
   
  val pos_blacklist: List[String] = List( "RB","RBS","RBR",
                                          "TO","DT","IN", "WDT"
                                        )
                                     
  val lexicon: List[String] = List(  //"can", "could", "may", "might", "must","shall", "should", "ought", "will", "would",  //modal verbs
                                     "lack", "wish", "suggest", "prefer", "rather",
                                     "expect", "need", "maybe", "perhaps", "hope",
                                     "fortunately", "unfortunately", "want", "miss",
                                     "look"
                                    )
  
  //EXPERIMENT 1
  def lexicon_innovation_detection(row: Row): Int = {
    
    //var sentence = row.getString(row.fieldIndex("sentence"))
    //var words = row.getList[String](row.fieldIndex("words"))
    var lemmas = row.getList[String](row.fieldIndex("lemmas"))
    
    // verify if lemma is in lexicon, to filter
    //lemmas.foreach{println} //debug
    for (ele <- lemmas) {
      if (lexicon.contains(ele)) {
        return 1
      }
    }                                     
    return 0
  }
  
  //EXPERIMENT 2
  def syntactic_pattern_innovation_detection(row: Row): Int = {
    
    //var sentence = row.getString(row.fieldIndex("sentence"))
    //var words = row.getList[String](row.fieldIndex("words"))
    var pos = row.getList[String](row.fieldIndex("pos"))
    
    // verify if lemma is in lexicon, to filter
    //pos.foreach{println}       //debug
    //println("============")    //debug
    
    //remove blacklist    
    for (ele <- pos) {
      if (pos_blacklist.contains(ele)) {
        pos = pos.filter(_ != ele)
      }
    }
    
    //pos.foreach{println}     //debug
    //println("************")  //debug
    
    val condensed_pos_list = pos.mkString("|")
    //println(condensed_pos_list)
    
    //verify if rules match   
    for (ele <- pos_rules) {
      if (condensed_pos_list.contains(ele)) {
        return 1
      }
    }
    
    return 0
  }
  
  //EXPERIMENT 3
  def lexicon_syntactic_composition_innovation_detection(row: Row): Int = {
    
    var inno_syntactic = row.getInt(row.fieldIndex("inno_syntactic"))
    var inno_lexicon = row.getInt(row.fieldIndex("inno_lexicon"))
    
    //rule.1: OR
    if (op == "OR") {
      if (inno_syntactic == 1 || inno_lexicon == 1) {
        return 1
      }
    }
    
    //rule.2: AND
    if (op == "AND") {
      if (inno_syntactic == 1 && inno_lexicon == 1) {
        return 1
      }
    }
    
    return 0
  }
  
  //EXPERIMENT 5
  def svm_syntactic_composition_innovation_detection(row: Row): Int = {
    
    var inno_syntactic = row.getInt(row.fieldIndex("inno_syntactic"))
    var inno_svm = row.getDouble(row.fieldIndex("prediction"))
    
    //rule.1: OR
    if (op == "OR") { 
      if (inno_syntactic == 1 || inno_svm == 1.0) {
        return 1
      }
    }
    
    //rule.2: AND
    if (op == "AND") {
      if (inno_syntactic == 1 && inno_svm == 1.0) {
        return 1
      }
    }
    
    return 0
  }
  
  //EXPERIMENT 6
  def svm_lexicon_composition_innovation_detection(row: Row): Int = {
    
    var inno_lexicon = row.getInt(row.fieldIndex("inno_lexicon"))
    var inno_svm = row.getDouble(row.fieldIndex("prediction"))
    
    //rule.1: OR
    if (op == "OR") {
      if (inno_lexicon == 1 || inno_svm == 1.0) {
        return 1
      }
    }
    
    //rule.2: AND
    if (op == "AND") {
      if (inno_lexicon == 1 && inno_svm == 1.0) {
        return 1
      }
    }
    
    return 0
  }
  
  //EXPERIMENT 7
  def svm_syntactic_lexicon_composition_innovation_detection(row: Row): Int = {
    
    var inno_syntactic = row.getInt(row.fieldIndex("inno_syntactic"))
    var inno_lexicon = row.getInt(row.fieldIndex("inno_lexicon"))
    var inno_svm = row.getDouble(row.fieldIndex("prediction"))
    
    //rule.1: OR
    if (op == "OR") {
      if (inno_syntactic == 1 || inno_lexicon == 1 || inno_svm == 1.0) {
        return 1
      }
    }
    
    //rule.2: AND
    if (op == "AND") {
      if (inno_syntactic == 1 && inno_lexicon == 1 && inno_svm == 1.0) {
        return 1
      }
    }
    
    return 0
  }
  
  //EXPERIMENT 8
  def innovation_syntactic_lexicon_svm_composition_innovation_detection(row: Row): Int = {
    
    var sentence = row.getString(row.fieldIndex("sentence"))
    //var inno_lexicon_syntactic = row.getInt(row.fieldIndex("inno_lexicon_syntactic"))
    var inno_svm = row.getDouble(row.fieldIndex("prediction"))
    
    //val qtd = lex_syn.get.filter((col("sentence") === sentence) && (col("inno_lexicon_syntactic") === 1)).count().asInstanceOf[Int]
    val qtd = lex_syn.get.filter(col("sentence") === sentence).count().asInstanceOf[Int]
  
    if (qtd == 1) {
      return inno_svm.asInstanceOf[Int]
    } else {
      return -1
    }
    
    return 0
  }
  
}



object Innovation {
  
  //main method of application
  def main(args: Array[String]): Unit = {
    
    // set the level of logs only for erros
    Logger.getLogger("org").setLevel(Level.ERROR)
  
    // starts de Spark Session
    val spark = SparkSession
        .builder()
        .appName("Experiment")
        .master("local[*]")                                      //unrequired to cluster run - just for debug
        .config("spark.driver.maxResultSize", "3g")
        .getOrCreate()  
    
    //addition of jars to the classpath                          //necessary to cluster run
    val sc = spark.sparkContext
    val master = sc.master
    if (master != "local[*]") {                                  //if running in a cluster, needs the resources folder clonned into each slave nodes
      val url_master = master.split(":")(1)                      //get just the //10.120.1.50 from master, passing via command line
      sc.addFile("hdfs:"+ url_master +":9000/resources", true)   //copy and paste the entire folder of resources from HDFS recursivelly (true) for each node  
    }
    
    //imports in loco
    import spark.implicits._                                     //allows the use of 'sentence like dataframe.col("sentence"), bitwise implicity conversions from RDD to DF and the contrary
    
    val input = args(0)
    var result: Option[DataFrame] = None
    
    val data0 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load(input).toDF("sentence","label"))
    
    val data = data0.withColumn("id", monotonically_increasing_id()).na.drop()
    
    // EXPERIMENT 1: innovation detection by syntactic pattern
        /*
        println("EXPERIMENT.1: Syntactic Pattern")
        
        //get POS
        val res0 = data.withColumn("pos", pos(data.col("sentence")))
        
        // Innovation detection method by syntactic pattern         
        val innovation_syntactic = spark.udf.register("Method", (row: Row) => { Execution.syntactic_pattern_innovation_detection(row) })
        result = Some(res0.withColumn("inno_syntactic", innovation_syntactic(struct(res0.columns.map(res0(_)) : _*))))
        //result.get.show()
        */
    // EXPERIMENT 2: innovation detection by innovative lexicon ===============================================================
        /*
        println("EXPERIMENT.2: Lexicon")
        
        //get lemmas
        val res1 = data.withColumn("lemmas", lemma(data.col("sentence")))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon = spark.udf.register("Method", (row: Row) => { Execution.lexicon_innovation_detection(row) })
        result = Some(res1.withColumn("inno_lexicon", innovation_lexicon(struct(res1.columns.map(res1(_)) : _*))))
        //result.get.show()  //debug
				*/				     
    // EXPERIMENT 3: innovation detection by combination of Exp. 1 and 2 ===============================================================
    /*
        println("EXPERIMENT.3: Syntactic Pattern and Lexicon Composition")
        
        //get POS
        var res1 = data.withColumn("pos", pos(data.col("sentence")))
        //get lemmas
        var res2 = res1.withColumn("lemmas", lemma(res1.col("sentence")))
        
        // Innovation detection method by syntactic pattern        
        val innovation_syntactic = spark.udf.register("Method", (row: Row) => { Execution.syntactic_pattern_innovation_detection(row) })
        res1 = res2.withColumn("inno_syntactic", innovation_syntactic(struct(res2.columns.map(res2(_)) : _*)))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon = spark.udf.register("Method", (row: Row) => { Execution.lexicon_innovation_detection(row) })
        res2 = res1.withColumn("inno_lexicon", innovation_lexicon(struct(res1.columns.map(res1(_)) : _*)))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon_syntactic_pattern_composition = spark.udf.register("Method", (row: Row) => { Execution.lexicon_syntactic_composition_innovation_detection(row) })
        result = Some(res2.withColumn("inno_lexicon_syntactic", innovation_lexicon_syntactic_pattern_composition(struct(res2.columns.map(res2(_)) : _*))))
        //result.get.show()  //debug
   	*/
    // EXPERIMENT 4: SVM classifier ======================================================================
    /*    
        println("EXPERIMENT.4: part 1 - SVM Classifier Trainning and Tuning")
        
        val res3 = data.na.drop()
        // Defines tokenizer
        val tokenizer = new Tokenizer()
                        .setInputCol("sentence")
                        .setOutputCol("words")
                        
        // Defines stop words remover                
        val remover = new StopWordsRemover()
              				.setInputCol("words")
              				.setOutputCol("filtered")
        
        // Creates and setup the TF-IDF
        val numFeatures = 5000
  		  val minDocFreq = 2
        
  		  // TF
        val hashingTF = new HashingTF()
                				.setInputCol("filtered")
                				.setOutputCol("tf")
                				.setNumFeatures(numFeatures)
  		  
        // IDF
  		  val idf = new IDF()
          				.setInputCol("tf")
          				.setOutputCol("features")
          				.setMinDocFreq(minDocFreq)
  		  
        // Creates the classifier instance
        val method = new LinearSVC()  				
          				
        // Pipeline
  		  val pipeline = new Pipeline().setStages(Array(tokenizer, remover, hashingTF, idf, method))
  		  
  		  // Creates and set grid for tunning and search for the best values in the Cross-validator.
  			// To create another classifier/method you need to search the Spark API docs for know the params and they value interval.	             
  		  val paramGrid = new ParamGridBuilder()
                				.addGrid(hashingTF.numFeatures, Array(10, 100, 1000, 2000, 3000, 4000, 5000))   // Number of TF features - for all methods. Do not remove
                				.addGrid(idf.minDocFreq, Array(1,2,3,4,5,6))                                    // Minimum frequency for IDF - for all methods. Do not remove
                				
                				//SVM parameters
                				.addGrid(method.maxIter, Array(10,15,20,25,30))
                				.addGrid(method.regParam, Array(0.0,0.1,0.2,0.3,0.4,0.5))
                				//.addGrid(method.tol, Array(0.0,0.1,0.2,0.3))
                				
                				.build()
  		  
  		  // Executes the cross validation with 10 dynamic splitted folds
  		  val cv = new CrossValidator()
        				 .setEstimator(pipeline)
        				 .setEvaluator(new BinaryClassificationEvaluator)
        				 .setEstimatorParamMaps(paramGrid)
        				 .setNumFolds(10)
        				 .setParallelism(2)
        				 
        try // If the model already exists, them load it
        {
          val cvModel = CrossValidatorModel.load("modelSVM")
        } 
        catch
        { // If the model doesn't exists, so create a new model.
          case _: Throwable => println("Model not find! Creating a new one.")
  			                       val cvModel = cv.fit(res3)    // Train
  			                       cvModel.save("modelSVM");     // Save the model
        }
        
    		*/
        // =================================================================================
        /*
        println("EXPERIMENT.4: part 2 - SVM Classifier Testing")
        
          val input2 = args(1)
          
          val data2 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input2).toDF("sentence","label"))
                       
        val dataTrainning = data.na.drop().withColumn("words", lemma(data.col("sentence")))
        val dataTesting = data2.na.drop().withColumn("words", lemma(data2.col("sentence")))
        
        //dataTrainning.show()
        //System.exit(0)
        
        // Define tokenizer
        /*val tokenizer = new Tokenizer()
                        .setInputCol("sentence")
                        .setOutputCol("words")
                        
        // Define stopwords remover                
        val remover = new StopWordsRemover()
              				.setInputCol("words")
              				.setOutputCol("filtered")
        */
        
        // Create and configure TF-IDF
        val numFeatures = 16384            //set value from Crossvalidation bestModel
  		  val minDocFreq = 1                //set value from Crossvalidation bestModel
        
  		  // TF
        val tf = new HashingTF()
          				.setInputCol("words")
          				.setOutputCol("tf")
          				.setNumFeatures(numFeatures)
  		  
        // IDF
  		  val idf = new IDF()
          				.setInputCol("tf")
          				.setOutputCol("idf")
          				.setMinDocFreq(minDocFreq)
        
        // Word2Vector
        val w2v = new Word2Vec()
        				  .setInputCol("words")
        				  .setOutputCol("w2v")
        				  .setVectorSize(16384)
        				  .setMinCount(1)
        
        val va = new VectorAssembler()
        				 .setInputCols(Array("idf","w2v"))
        				 .setOutputCol("features_vector")
    
        val selector = new ChiSqSelector()
                       .setNumTopFeatures(20)
                       .setFeaturesCol("features_vector")
                       .setLabelCol("label")
                       .setOutputCol("features")
  		  
  		  // Load instance of Linear SVM classifier 
  		  val svm = new LinearSVC()
                      .setRegParam(1.0)
                      .setAggregationDepth(2)
                      .setMaxIter(500)
  		  
  		  val pipeline = new Pipeline()
  				             .setStages(Array(tf, idf, w2v, va, selector, svm))
  		  
        val model = pipeline.fit(dataTrainning)
        model.save(input2 + ".model")
  				             
  			//run the classification
  		  val testing = model.transform(dataTesting)
  		  
  		  testing.filter(testing.col("prediction") === 1).show()
  		  result = Some(testing)
    */
  	// EXPERIMENT 5: SVM Classifier + syntactic pattern ===============================================================
    /*
    println("EXPERIMENT.5: SVM Classifier and Syntactic Pattern")
        
        //args(1) = trainning set
        //args(0) = test set
        
          val input2 = args(1)
          
          val data2 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input2).toDF("sentence","label"))
                       
        //val dataTrainning = data.na.drop().withColumn("words", lemma(data.col("sentence")))
        val dataTesting = data2.na.drop().withColumn("words", lemma(data2.col("sentence")))
        
        Execution.op = "AND"                                         //selection of rule variance
  			val model = PipelineModel.load("data/INNOVATION/model5")    //selection of model to execute
        
  			//run the classification
  		  val testing = model.transform(dataTesting)
  		  
  		  //get POS
        var res1 = testing.withColumn("pos", pos(data2.col("sentence")))
        //get lemmas
        var res2 = res1.withColumn("lemmas", lemma(res1.col("sentence")))
        
        // Innovation detection method by syntactic pattern        
        val innovation_syntactic = spark.udf.register("Method", (row: Row) => { Execution.syntactic_pattern_innovation_detection(row) })
        res1 = res2.withColumn("inno_syntactic", innovation_syntactic(struct(res2.columns.map(res2(_)) : _*)))
  		  
  		  //res1.show()
  		  //result = Some(testing)
        
        // Innovation detection method by lexicon         
        val innovation_svm_syntactic_pattern_composition = spark.udf.register("Method", (row: Row) => { Execution.svm_syntactic_composition_innovation_detection(row) })
        result = Some(res1.withColumn("inno_svm_syntactic", innovation_svm_syntactic_pattern_composition(struct(res1.columns.map(res1(_)) : _*))))
        result.get.show()  //debug
    */
   	
    // EXPERIMENT 6: SVM Classifier + lexicon ===============================================================
    /*
    println("EXPERIMENT.6: SVM Classifier and Lexicon")
        
        //args(0) = test set
        //args(1) = trainning set
        
          val input2 = args(1)
          
          val data2 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input2).toDF("sentence","label"))
        
        val dataTrainning = data2.na.drop()
        val dataTesting = data2.na.drop().withColumn("words", lemma(data2.col("sentence")))
        
        Execution.op = "AND"                         								//selection of rule variance
  			val model = PipelineModel.load("data/INNOVATION/model5")    //selection of model to execute
  			//run the classification
  		  val testing = model.transform(dataTesting)
  		  
  		  //get POS
        var res1 = testing.withColumn("pos", pos(data2.col("sentence")))
        //get lemmas
        var res2 = res1.withColumn("lemmas", lemma(res1.col("sentence")))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon = spark.udf.register("Method", (row: Row) => { Execution.lexicon_innovation_detection(row) })
        res1 = res2.withColumn("inno_lexicon", innovation_lexicon(struct(res2.columns.map(res2(_)) : _*)))
  		  
  		  //res1.show()
  		  //result = Some(testing)
        
        // Innovation detection method by lexicon         
        val innovation_svm_lexicon_pattern_composition = spark.udf.register("Method", (row: Row) => { Execution.svm_lexicon_composition_innovation_detection(row) })
        result = Some(res1.withColumn("inno_svm_lexicon", innovation_svm_lexicon_pattern_composition(struct(res1.columns.map(res1(_)) : _*))))
        result.get.show()  //debug	  
  	*/
    
    // EXPERIMENT 7: SVM Classifier + syntactic pattern + lexicon ===============================================================
    
    println("EXPERIMENT.7: SVM Classifier, Syntactic Pattern and Lexicon")
        
        //args(0) = test set
        //args(1) = trainning set
        
          val input2 = args(1)
          
          val data2 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input2).toDF("sentence","label"))
        
        val dataTrainning = data2.na.drop()
        val dataTesting = data2.na.drop().withColumn("words", lemma(data2.col("sentence")))
        
        Execution.op = "AND"                        									//selection of rule variance
  			val model = PipelineModel.load("data/INNOVATION/model5")    //selection of model to execute
  			
  			//run the classification
  		  val testing = model.transform(dataTesting)
  		  
  		  //get POS
        var res1 = testing.withColumn("pos", pos(data2.col("sentence")))
        //get lemmas
        var res2 = res1.withColumn("lemmas", lemma(res1.col("sentence")))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon = spark.udf.register("Method", (row: Row) => { Execution.lexicon_innovation_detection(row) })
        res1 = res2.withColumn("inno_lexicon", innovation_lexicon(struct(res2.columns.map(res2(_)) : _*)))
  		  
        // Innovation detection method by syntactic pattern        
        val innovation_syntactic = spark.udf.register("Method", (row: Row) => { Execution.syntactic_pattern_innovation_detection(row) })
        res2 = res1.withColumn("inno_syntactic", innovation_syntactic(struct(res1.columns.map(res1(_)) : _*)))
        
  		  //res1.show()
  		  //result = Some(testing)
        
        // Innovation detection method by lexicon         
        val innovation_svm_syntactic_lexicon_pattern_composition = spark.udf.register("Method", (row: Row) => { Execution.svm_syntactic_lexicon_composition_innovation_detection(row) })
        result = Some(res2.withColumn("inno_svm_syntactic_lexicon", innovation_svm_syntactic_lexicon_pattern_composition(struct(res2.columns.map(res2(_)) : _*))))
        result.get.show()  //debug
    
    
    // EXPERIMENT 8: syntactic pattern + lexicon (OR) --> SVM Classifier ===============================================================
    /*
    println("EXPERIMENT.8: Syntactic Pattern OR Lexicon --> SVM Classifier")
        
          val input2 = args(1)
          val input3 = args(2)
          //val input4 = args(3)
          
          val data2 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input2).toDF("sentence","label"))
          
          val data3 = (spark.read.format("csv")
                       .option("header","false")
                       .option("delimiter","\t")
                       .option("inferSchema","true")
                       .load(input3).toDF("sentence","label","inno_lexicon","inno_syntactic","inno_lexicon_syntactic"))
                       //.load(input3).toDF("sentence","label"))
        
                       
        Execution.lex_syn = Some(data3.na.drop())
        //val dataFull = data3.na.drop()
        val dataTrainning = data2.na.drop()
        val dataTesting = data2.na.drop().withColumn("words", lemma(data2.col("sentence")))
        
        /*
        Execution.op = "OR"                         //selection of rule variance
        
  		  //get POS
        var res1 = dataFull.withColumn("pos", pos(dataFull.col("sentence")))
        //get lemmas
        var res2 = res1.withColumn("lemmas", lemma(res1.col("sentence")))
        
        // Innovation detection method by lexicon         
        val innovation_lexicon = spark.udf.register("Method", (row: Row) => { Execution.lexicon_innovation_detection(row) })
        res1 = res2.withColumn("inno_lexicon", innovation_lexicon(struct(res2.columns.map(res2(_)) : _*)))
  		  
        // Innovation detection method by syntactic pattern        
        val innovation_syntactic = spark.udf.register("Method", (row: Row) => { Execution.syntactic_pattern_innovation_detection(row) })
        res2 = res1.withColumn("inno_syntactic", innovation_syntactic(struct(res1.columns.map(res1(_)) : _*)))
        
        // Innovation detection method by lexicon syntactic composition (OR)         
        val innovation_lexicon_syntactic_pattern_composition = spark.udf.register("Method", (row: Row) => { Execution.lexicon_syntactic_composition_innovation_detection(row) })
        res1 = res2.withColumn("inno_lexicon_syntactic", innovation_lexicon_syntactic_pattern_composition(struct(res2.columns.map(res2(_)) : _*)))
        
  		  result = Some(res1)
  		  //result = Some(testing)
        */
        
        Execution.op = "OR"                         //selection of rule variance
  			val model = PipelineModel.load("data/INNOVATION/model1")    //selection of model to execute
  			
  			//run the classification
  		  val testing = model.transform(dataTesting)
        
  		  val innovation_syntactic_lexicon_svm_composition = spark.udf.register("Method", (row: Row) => { Execution.innovation_syntactic_lexicon_svm_composition_innovation_detection(row) })
        val res1 = testing.withColumn("inno_syntactic_lexicon_svm", innovation_syntactic_lexicon_svm_composition(struct(testing.columns.map(testing(_)) : _*)))
        val res2 = res1.filter(col("inno_syntactic_lexicon_svm") >= 0)
        result = Some(res2)
        result.get.show()  //debug
    */		
        
        
        
        
        
        
    //WRITE RESULTS ========================================================================================
    /*
    val stringify = udf((vs: Seq[String]) => s"""[${vs.flatMap(Option[String]).mkString(",")}]""")
    //result = Some(result.get.withColumn("wordsString", stringify('words)))
    //result = Some(result.get.withColumn("lemmasString", stringify('lemmas)))
    
    result.get                  //.get to remove Some of Option[DataFrame]
    .select("sentence", "label", "inno_lexicon_syntactic")
    .na.drop()                    //ignore regs with null
    //.rdd.saveAsTextFile(saida)  //alternative to save the result
    .write
    .mode("overwrite")
    .format("csv")
    .option("delimiter", "\t")
    .save("data/inno_lexicon_syntactic.out")
    */

    
    //METRICS CALC ========================================================================================= 
      
      // Experiment 1 - Syntactic Pattern
      /*  
      val predictions = result.get.select( when($"inno_syntactic" === 0, 0.0)
                                          .when($"inno_syntactic" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))
      */
        
      // Experiment 2 - Lexicon
      /*
		  val predictions = result.get.select( when($"inno_lexicon" === 0, 0.0)
                                          .when($"inno_lexicon" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))
		  */
		  
      // Experiment 3 - Composition Lexicon and Syntactic  
      /*  
      val predictions = result.get.select( when($"inno_lexicon_syntactic" === 0, 0.0)
                                          .when($"inno_lexicon_syntactic" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))  
      */
  		  
      // Experiment 4 - part 2 - SVM Classifier Testing  
      /*
      val predictions = result.get.select( when($"prediction" === 0, 0.0)
                                          .when($"prediction" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))
      */
		  
		  // Experiment 5 - SVM Classifier + Syntactic Pattern  
      /*  
      val predictions = result.get.select( when($"inno_svm_syntactic" === 0, 0.0)
                                          .when($"inno_svm_syntactic" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))
		  */
      
      // Experiment 6 - SVM Classifier + Lexicon 
      /*
      val predictions = result.get.select( when($"inno_svm_lexicon" === 0, 0.0)
                                          .when($"inno_svm_lexicon" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))
		  */
      
      // Experiment 7 - SVM Classifier + Syntactic Pattern + Lexicon 
      
  	  val predictions = result.get.select( when($"inno_svm_syntactic_lexicon" === 0, 0.0)
                                          .when($"inno_svm_syntactic_lexicon" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))  
		  
      // Experiment 8 - Syntactic Pattern + Lexicon --> SVM Classifier 
      /*
  	  val predictions = result.get.select( when($"inno_syntactic_lexicon_svm" === 0, 0.0)
                                          .when($"inno_syntactic_lexicon_svm" === 1, 1.0)
                                          .as("prediction"))
		                                      .rdd.map(_.getDouble(0))  
		  */ 
      val labels      = result.get.select( when($"label" === 0, 0.0)
                                          .when($"label" === 1, 1.0)
                                          .as("label"))
                                          .rdd.map(_.getDouble(0))
    
      val predictionAndLabels = predictions.zip(labels)
      val pl = predictionAndLabels.collect()
  	  
      // Instantiate metrics object
      val metrics = new BinaryClassificationMetrics(predictionAndLabels)
      
      // Precision by threshold
      val precision = metrics.precisionByThreshold
      precision.foreach { case (t, p) =>
        //println(s"Threshold: $t, Precision: $p")
        println(s"$p")
      }
      
      // Recall by threshold
      val recall = metrics.recallByThreshold
      recall.foreach { case (t, r) =>
        //println(s"Threshold: $t, Recall: $r")
        println(s"$r")
      }
      
      // Precision-Recall Curve
      val PRC = metrics.pr
      
      // F-measure
      val f1Score = metrics.fMeasureByThreshold
      f1Score.foreach { case (t, f) =>
        //println(s"Threshold: $t, F-score: $f, Beta = 1")
        println(s"$f")
      }
      
      //val beta = 0.5
      //val fScore = metrics.fMeasureByThreshold(beta)
      //f1Score.foreach { case (t, f) =>
      //  println(s"Threshold: $t, F-score: $f, Beta = 0.5")
        //println(s"$f")
      //}
      
      // AUPRC
      //val auPRC = metrics.areaUnderPR
      //println("Area under precision-recall curve = " + auPRC)
      //println(auPRC)
      
      // Compute thresholds used in ROC and PR curves
      //val thresholds = precision.map(_._1)
      
      // ROC Curve
      //val roc = metrics.roc
      
      // AUROC
      //val auROC = metrics.areaUnderROC
      //println("Area under ROC = " + auROC)
      //println(auROC)
    
    
    
    
    spark.stop()
    
    // Register the DataFrame as a SQL temporary view
    /*res.createOrReplaceTempView("results")
    var sql = "SELECT * FROM results WHERE sentence LIKE '%would%' OR sentence LIKE '%will%'"
    res = spark.sql(sql)
    res.show()
    System.exit(1)*/
  }  
}