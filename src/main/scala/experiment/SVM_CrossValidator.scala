package experiment

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import com.databricks.spark.corenlp.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import scala.collection.JavaConversions._  //to transparent convert java.util.List to scala.List
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.{HashingTF, IDF, StopWordsRemover}

import org.apache.spark.ml.feature.ChiSqSelector
import org.apache.spark.ml.feature.IDF
import org.apache.spark.ml.feature.NGram
import org.apache.spark.ml.feature.OneHotEncoder
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.feature.Word2Vec
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.ml.param.ParamMap

import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.ml.classification.LinearSVC
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.PipelineModel
import com.johnsnowlabs.nlp.base._
import com.johnsnowlabs.nlp.annotator._
import com.johnsnowlabs.nlp.pretrained.pipelines.en.BasicPipeline
import com.johnsnowlabs.nlp.Finisher

object SVM_CrossValidator {
  
  //main method of application
  def main(args: Array[String]): Unit = {
    
    // ========================================================================
    // SPARK INITIALIZE
    // ========================================================================
    // set the level of logs only for erros
    Logger.getLogger("org").setLevel(Level.ERROR)
  
    // starts de Spark Session
    val spark = SparkSession
        .builder()
        .appName("Experiment")
        .master("local[*]")                                      //unrequired to cluster run - just for debug
        //.config()
        .getOrCreate()
    
    //imports in loco
    import spark.implicits._                                     //allows the use of 'sentence like dataframe.col("sentence"), bitwise implicity conversions from RDD to DF and the contrary
    
    // ========================================================================
    // INPUT
    // ========================================================================
    val input1 = "data/INNOVATION/treinamento1.csv"
    val input2 = "data/INNOVATION/teste1.csv"
    
    var result: Option[DataFrame] = None
    
    val data0 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load(input1).toDF("sentence","label"))
          
    val data1 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load(input2).toDF("sentence","label"))             
    
    // entire dataset
    //val splits = data0.randomSplit(Array(0.8, 0.2), seed = 1234L)  
    //val dataTrainning = splits(0).withColumn("words", lemma(splits(0).col("sentence")))
    //val dataTesting = splits(1).withColumn("words", lemma(splits(1).col("sentence")))
                 
    // partition dataset            
    val dataTrainning = data0.na.drop().withColumn("words", lemma(data0.col("sentence")))
    val dataTesting = data1.na.drop().withColumn("words", lemma(data1.col("sentence")))
    
    // ========================================================================
    // STEPS
    // ========================================================================
    /*
    val res0 = BasicPipeline().annotate(dataTrainning, "sentence")
    
    val stemmer = new Stemmer()
                      .setInputCols(Array("token"))
                      .setOutputCol("stem")
    
    val res1 = stemmer.transform(res0) 
      
    val finisher = new Finisher()
                       .setInputCols("token", "normal", "lemma", "pos", "stem")

    val res2 = finisher.transform(res1)  //sai do annotator do SparkNLP e retorna o DF
		*/
    val tf = new HashingTF()
        				 .setInputCol("words")
        				 .setOutputCol("tf")
        				 .setNumFeatures(16384)
    
    val idf = new IDF()
      				    .setInputCol("tf")
      				    .setOutputCol("idf")
      				    .setMinDocFreq(1)
      				    
    val w2v = new Word2Vec()
        				  .setInputCol("words")
        				  .setOutputCol("w2v")
        				  .setNumPartitions(1)
        				  .setMaxIter(1)
        				  .setVectorSize(16384)
        				  .setSeed(0)
        				  .setMinCount(5)
    
    val ohe = new OneHotEncoder()
          				.setInputCol("label")
          				.setOutputCol("ohe")
    
    val va = new VectorAssembler()
        				 .setInputCols(Array("idf","w2v","ohe"))
        				 .setOutputCol("features")
	  
	  // Load instance of Linear SVM classifier 
	  val svm = new LinearSVC()
                  .setRegParam(1.0)
                  .setAggregationDepth(2)
                  .setMaxIter(500)
                  .setThreshold(0.0)
                  .setTol(1E-6)
                  
	  
	  val pipeline = new Pipeline()
			             .setStages(Array(tf, idf, w2v, ohe, va, svm))
	  
    //val model = pipeline.fit(dataTrainning)
		
		// Creates and set grid for tunning and search for the best values in the Cross-validator.
		// To create another classifier/method you need to search the Spark API docs for know the params and they value interval.	             
	  val paramGrid = new ParamGridBuilder()
    /*
            				.addGrid(tf.numFeatures, Array(1000,5000,10000,13000, 16384, 20000))   
            				.addGrid(idf.minDocFreq, Array(0,1,2,3,4,5,6,7,8,9,10))   
            				//Word2Vec parameters
            				.addGrid(w2v.maxIter, Array(0,1,2,3,4,5,6,7,8,9,10))
            				.addGrid(w2v.vectorSize, Array(1000,5000,10000,13000,16384,20000))
            				.addGrid(w2v.numPartitions, Array(1,2))
            				.addGrid(w2v.minCount, Array(4,5,6))
            				//SVM parameters
            				.addGrid(svm.maxIter, Array(10,100, 250, 500, 600))
            				.addGrid(svm.regParam, Array(0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9))
            				
    */
    
                    .addGrid(tf.numFeatures, Array(10))   
            				.addGrid(idf.minDocFreq, Array(0))   
            				//Word2Vec parameters
            				.addGrid(w2v.maxIter, Array(10))
            				.addGrid(w2v.vectorSize, Array(10))
            				.addGrid(w2v.numPartitions, Array(1))
            				.addGrid(w2v.minCount, Array(4))
            				//SVM parameters
            				.addGrid(svm.maxIter, Array(10))
            				.addGrid(svm.regParam, Array(0.1))
            				.build()
	  
	  // Executes the cross validation with 10 dynamic splitted folds
	  val cv = new CrossValidator()
    				 .setEstimator(pipeline)
    				 .setEvaluator(new BinaryClassificationEvaluator)
    				 .setEstimatorParamMaps(paramGrid)
    				 .setNumFolds(10)
    				 .setParallelism(16)
    	
    var cvModel: CrossValidatorModel = null				 
    				 
    try // If the model already exists, them load it
    {
      cvModel = CrossValidatorModel.load("modelSVM")
    } 
    catch
    { // If the model doesn't exists, so create a new model.
      case _: Throwable => println("Model not find! Creating a new one.")
		                       cvModel = cv.fit(dataTrainning)    // Train
		                       cvModel.save("modelSVM");     // Save the model
    }
    
    val model = cvModel.bestModel
			             
		//run the classification
	  val testing = model.transform(dataTesting)
	  
	  testing.show()
	  result = Some(testing)
    
    // =============================================================================
	  // METRICS
	  // =============================================================================
	  val predictions = result.get.select( when($"prediction" === 0, 0.0)
                                        .when($"prediction" === 1, 1.0)
                                        .as("prediction"))
	                                      .rdd.map(_.getDouble(0))
	        
	  val labels      = result.get.select( when($"label" === 0, 0.0)
                                        .when($"label" === 1, 1.0)
                                        .as("label"))
                                        .rdd.map(_.getDouble(0))
                                        
    val predictionAndLabels = predictions.zip(labels)
    val pl = predictionAndLabels.collect()
	  
    // Instantiate metrics object
    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
    
    // Precision by threshold
    val precision = metrics.precisionByThreshold
    precision.foreach { case (t, p) =>
      println(s"Threshold: $t, Precision: $p")
      //println(s"$p")
    }
    
    // Recall by threshold
    val recall = metrics.recallByThreshold
    recall.foreach { case (t, r) =>
      println(s"Threshold: $t, Recall: $r")
      //println(s"$r")
    }
    
    // F-measure
    val f1Score = metrics.fMeasureByThreshold
    f1Score.foreach { case (t, f) =>
      println(s"Threshold: $t, F-score: $f, Beta = 1")
      //println(s"$f")
    }

    spark.stop()
  }
}