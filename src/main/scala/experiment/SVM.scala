/*
 * How to make build with sbt:
 * 	1) Save project in eclipse (without .master([*]) and without a copy of resources for workers)
 * 	2) Execute cmd/console at root folder of project
 * 	3) Execute commands:
 * 		a) sbt clean
 * 	  b) sbt reload
 *    c) set JAVA_OPTS=-Xmx4G (Windows) or export SBT_OPTS="-Xmx4G" (Linux) to increase heap of JVM 
 * 		c) sbt assembly
 * 	4) Take the jar file generated in target folder as your final bigfeel jar file
 * 	5) To treat duplicated files in assembly: add instructions of merge in build.sbt https://stackoverflow.com/questions/25144484/sbt-assembly-deduplication-found-error
*/
package experiment

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import com.databricks.spark.corenlp.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import scala.collection.JavaConversions._  //to transparent convert java.util.List to scala.List
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.{HashingTF, IDF, StopWordsRemover}

import org.apache.spark.ml.feature.ChiSqSelector
import org.apache.spark.ml.feature.IDF
import org.apache.spark.ml.feature.NGram
import org.apache.spark.ml.feature.OneHotEncoder
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.feature.Word2Vec
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.ml.param.ParamMap

import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.ml.classification.LinearSVC
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.PipelineModel
import com.johnsnowlabs.nlp.base._
import com.johnsnowlabs.nlp.annotator._
import com.johnsnowlabs.nlp.pretrained.pipelines.en.BasicPipeline
import com.johnsnowlabs.nlp.Finisher

object SVM {
  
  //main method of application
  def main(args: Array[String]): Unit = {
    
    // ========================================================================
    // SPARK INITIALIZE
    // ========================================================================
    // set the level of logs only for erros
    Logger.getLogger("org").setLevel(Level.ERROR)
  
    // starts de Spark Session
    val spark = SparkSession
        .builder()
        .appName("SVM")
        .master("local[*]")                                      //unrequired to cluster run - just for debug
        .config("spark.driver.maxResultSize", "2g")
        .getOrCreate()
    
    //imports in loco
    import spark.implicits._                                     //allows the use of 'sentence like dataframe.col("sentence"), bitwise implicity conversions from RDD to DF and the contrary
    
    // ========================================================================
    // INPUT
    // ========================================================================
    val input1 = /*"data/INNOVATION/trainning1.csv"*/ args(0)
    val input2 = /*"data/INNOVATION/teste1.csv"*/ args(1)
    
    var result: Option[DataFrame] = None
    
    val data0 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load(input1).toDF("sentence","label"))
    
    
    val data1 = (spark.read.format("csv")
                 .option("header","false")
                 .option("delimiter","\t")
                 .option("inferSchema","true")
                 .load(input2).toDF("sentence","label"))             
    
    
    // entire dataset
    val splits = data0.randomSplit(Array(0.8, 0.2), seed = 1234L)  
    val dataTrainning = splits(0).na.drop().withColumn("words", lemma(splits(0).col("sentence")))
    val dataTesting = splits(1).na.drop().withColumn("words", lemma(splits(1).col("sentence")))
                 
    // partition dataset            
    //val dataTrainning = data0.na.drop().withColumn("words", lemma(data0.col("sentence")))
    //val dataTesting = data1.na.drop().withColumn("words", lemma(data1.col("sentence")))
    
    // ========================================================================
    // STEPS
    // ========================================================================
    /*
    val res0 = BasicPipeline().annotate(dataTrainning, "sentence")
    
    val stemmer = new Stemmer()
                      .setInputCols(Array("token"))
                      .setOutputCol("stem")
    
    val res1 = stemmer.transform(res0) 
      
    val finisher = new Finisher()
                       .setInputCols("token", "normal", "lemma", "pos", "stem")

    val res2 = finisher.transform(res1)  //sai do annotator do SparkNLP e retorna o DF
		*/
    val tf = new HashingTF()
        				 .setInputCol("words")
        				 .setOutputCol("tf")
        				 .setNumFeatures(16384)
    
    val idf = new IDF()
      				    .setInputCol("tf")
      				    .setOutputCol("idf")
      				    .setMinDocFreq(1)
      				    
    val w2v = new Word2Vec()
        				  .setInputCol("words")
        				  .setOutputCol("w2v")
        				  //.setVectorSize(16384)
        				  .setMinCount(0)
    
    val va = new VectorAssembler()
        				 .setInputCols(Array("idf","w2v"))
        				 .setOutputCol("features_vector")
        				 
    val selector = new ChiSqSelector()
                   .setNumTopFeatures(10)
                   .setFeaturesCol("features_vector")
                   .setLabelCol("label")
                   .setOutputCol("features")
	  
	  // Load instance of Linear SVM classifier 
	  val svm = new LinearSVC()
                  .setRegParam(1.0)
                  .setAggregationDepth(2)
                  .setMaxIter(500)
                  .setThreshold(0.0)
                  .setTol(1E-6)
	  
	  val pipeline = new Pipeline()
			             .setStages(Array(tf, idf, w2v, va, selector, svm))
	  
		val paramGrid = new ParamGridBuilder()
                    .addGrid(tf.numFeatures, Array(16384))   
            				.addGrid(idf.minDocFreq, Array(1))   
            				//Word2Vec parameters
            				.addGrid(w2v.vectorSize, Array(16384))
            				.addGrid(w2v.minCount, Array(1))
            				//ChiSqSelector parameters
            				.addGrid(selector.numTopFeatures, Array(10))
            				//SVM parameters
            				.addGrid(svm.maxIter, Array(15))
            				.addGrid(svm.regParam, Array(0.3))
            				.build()
	 
    // Executes the cross validation with 10 dynamic splitted folds
	  val cv = new CrossValidator()
    				 .setEstimator(pipeline)
    				 .setEvaluator(new BinaryClassificationEvaluator)
    				 .setEstimatorParamMaps(paramGrid)
    				 .setNumFolds(5)
    				 .setParallelism(2)
    	
    var cvModel: CrossValidatorModel = null		             
    
    cvModel = cv.fit(dataTrainning)              //trainning
    val model = cvModel.bestModel                //get the best model
    cvModel.save(input2 + ".modelSVM");          //save model
	  val testing = model.transform(dataTesting)   //testing
	  
	  //testing.show()
	  result = Some(testing)
    
    // =============================================================================
	  // METRICS
	  // =============================================================================
	  val predictions = result.get.select( when($"prediction" === 0, 0.0)
                                        .when($"prediction" === 1, 1.0)
                                        .as("prediction"))
	                                      .rdd.map(_.getDouble(0))
	        
	  val labels      = result.get.select( when($"label" === 0, 0.0)
                                        .when($"label" === 1, 1.0)
                                        .as("label"))
                                        .rdd.map(_.getDouble(0))
                                        
    val predictionAndLabels = predictions.zip(labels)
    val pl = predictionAndLabels.collect()
	  
    // Instantiate metrics object
    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
    
    // Precision by threshold
    val precision = metrics.precisionByThreshold
    precision.foreach { case (t, p) =>
      println(s"Threshold: $t, Precision: $p")
      //println(s"$p")
    }
    
    // Recall by threshold
    val recall = metrics.recallByThreshold
    recall.foreach { case (t, r) =>
      println(s"Threshold: $t, Recall: $r")
      //println(s"$r")
    }
    
    // F-measure
    val f1Score = metrics.fMeasureByThreshold
    f1Score.foreach { case (t, f) =>
      println(s"Threshold: $t, F-score: $f, Beta = 1")
      //println(s"$f")
    }

    spark.stop()
  }
}